/*****************************************************************************
Copyright: 
File name: /contrib/cad.c
Description: This is an extension for feature recognition. 
Author: Zhibin Niu
Version: 
Date: 
History: 
*****************************************************************************/


#include "postgres.h"
#include "fmgr.h"
#include <string.h>
#include "cfiStandardFun.h"
#include "miscadmin.h"
#include "catalog/pg_type.h"
#include "utils/array.h"

#include <stddef.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>      
#include <dirent.h>

#include "cadapi.c"

#include <stdlib.h>
#include "engine.h"
#define  BUFSIZE 256


#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

#define _USE_MATH_DEFINES
#include <math.h>
#include <stddef.h>

#define ERR(__function__) {result = (__function__); if (result) goto cleanup;}

enum dihedral_class {
    CONCAVE=1,
    CONVEX,
    TANGENTIAL,
    MIXED
};

#include "timers.h"
int result = 0;



bool learn_perimeter();


int face_orientation(int face, double *orientation);

int classify(int line, int *class);
static int classify_at_t(int face1, int face2, int line,
    double orient_line_in_face1, double t, int *class);

/* Classifies a given line as CONCAVE, CONVEX, TANGENTIAL or MIXED. */
int classify(int line, int *class) {
    int num_parents;
    int *parent_types = NULL;
    int *signed_parent_numbers = NULL;
    int magic;
    int num_parents2;
    int face1;
    int face2;
    int orient_line_in_face1;
    int i;

    *class = 0;

    cficGetTopoParentTotal(CFI_TYPE_LINE, line, &num_parents);

    if (num_parents != 2) {
        /* Line is non-manifold - can't get a dihedral angle. */
        return -1;
    }

    magic = 0;
    cficGetTopoParentList(CFI_TYPE_LINE, line, num_parents, &magic,
        &num_parents2, &parent_types, &signed_parent_numbers);

    if (parent_types[0] != CFI_TYPE_FACE
        || parent_types[1] != CFI_TYPE_FACE) {
        /* This line has an unexpected parency. */
        return -2;
    }

    face1 = abs(signed_parent_numbers[0]);
    orient_line_in_face1 = signed_parent_numbers[0] > 0 ? 1.0 : -1.0;
    face2 = abs(signed_parent_numbers[1]);

    /* Check the classification at 11 points along the edge. */
    for (i = 0; i <= 10; ++i) {
        int class2;

        classify_at_t(face1, face2, line, orient_line_in_face1, i * 0.1, &class2);

        /* If we have no classification for this edge yet, use this
           classification. Otherwise, if the current classification is
           tangential, override it with the new one, or if the current and new
           classifications disagree, mark this edge as MIXED. */
        if (*class == 0) {
            *class = class2;
        } else if (*class == class2) {
            /* Do nothing. */
        } else if (*class == TANGENTIAL) {
            *class = class2;
        } else {
            *class = MIXED;
        }
    }

 // cleanup:
 //   if (parent_types) cficFree(parent_types);
 //   if (signed_parent_numbers) cficFree(signed_parent_numbers);

    return 0;
}

static double dot_product(double v1[3], double v2[3]);
static void cross_product(double v1[3], double v2[3], double v[3]);

int classify_at_t(int face1, int face2, int line,
    double orient_line_in_face1, double t, int *class) {

    

    double *uv = NULL;
    double *normal1 = NULL;
    double *normal2 = NULL;
    double orient1;
    double orient2;
    double cross[3];
    double tangent[3];
    double dxtt, dytt, dztt;
    double cosine;
    int convex;

    ERR(cficCalcFaceUVFromEdgeT(face1, line, t, &uv));
    ERR(cficCalcFaceNormalAtUV(face1, uv, &normal1));
    cficFree(uv);
    uv = NULL;
    ERR(cficCalcFaceUVFromEdgeT(face2, line, t, &uv));
    ERR(cficCalcFaceNormalAtUV(face2, uv, &normal2));

    /* Take account of the orientations of the faces within their parent body -
     * reverse the normal if the face is used in a negative sense. */
    ERR(face_orientation(face1, &orient1));
    ERR(face_orientation(face2, &orient2));

    {
        int j;
        for (j = 0; j < 3; ++j) {
            normal1[j] *= orient1;
            normal2[j] *= orient2;
        }
    }

    /* Find the cosine of the angle between the normals, to determine if the
     * angle is flat. */
    cosine = dot_product(normal1, normal2);

    /* Take the cross-product of the two normals, and compare it with the line
     * tangent, to see if it points in the same direction. This allows us to
     * tell concavity from convexity.
     * The sense of the line in the parent faces, and the orientation of the
     * parent faces in the bodies, must be taken into account. */
    cross_product(normal1, normal2, cross);

    cficCalcLineDerivAtT(line, t, tangent + 0, tangent + 1,
        tangent + 2, &dxtt, &dytt, &dztt);

    convex = dot_product(cross, tangent) * orient_line_in_face1 * orient1 < 0.0;

    if (fabs(cosine) > cos(M_PI / 40.0)) {
        /* Angle < 4.5 degrees; call this flat. */
        *class = TANGENTIAL;
    } else if (convex) {
        *class = CONVEX;
    } else {
        *class = CONCAVE;
    }

  cleanup:
    if (uv) cficFree(uv);
    if (normal1) cficFree(normal1);
    if (normal2) cficFree(normal2);

    return result;
}


/* Returns the orientation of a face within its parent body. */
int face_orientation(int face, double *orientation) {
    int result = 0;

    int num_parents;
    int magic;
    int *parent_types = NULL;
    int *signed_parent_numbers = NULL;
    int num_parents2;

    ERR(cficGetTopoParentTotal(CFI_TYPE_FACE, face, &num_parents));

    if (num_parents != 1) {
        /* Face is non-manifold - can't get a dihedral angle. */
        result = -1;
        goto cleanup;
    }

    magic = 0;
    ERR(cficGetTopoParentList(CFI_TYPE_FACE, face, num_parents, &magic,
            &num_parents2, &parent_types, &signed_parent_numbers));

    if (parent_types[0] != CFI_TYPE_BODY) {
        /* This face lives in a combined face. Unsupported by this code. */
        result = -2;
        goto cleanup;
    }

    if (signed_parent_numbers[0] > 0) {
        *orientation = 1.0;
    } else {
        *orientation = -1.0;
    }

  cleanup:
    if (parent_types != NULL) cficFree(parent_types);
    if (signed_parent_numbers != NULL) cficFree(signed_parent_numbers);

    return result;
}

double dot_product(double v1[3], double v2[3]) {
    int j;
    double dot = 0.0;

    for (j = 0; j < 3; ++j) {
        dot += v1[j] * v2[j];
    }

    return dot;
}

void cross_product(double v1[3], double v2[3], double v[3]) {
    double vt[3];

    vt[0] = v1[1]*v2[2] - v1[2]*v2[1];
    vt[1] = v1[2]*v2[0] - v1[0]*v2[2];
    vt[2] = v1[0]*v2[1] - v1[1]*v2[0];

    v[0] = vt[0];
    v[1] = vt[1];
    v[2] = vt[2];
}



PG_FUNCTION_INFO_V1( cadinit );
Datum cadinit(PG_FUNCTION_ARGS);
Datum cadinit(PG_FUNCTION_ARGS)
{

  int mode;
  char *version;
  char *fixno;
  int result = cficInit(&mode, &version, &fixno);
  cficFree(version);
  cficFree(fixno);

  if(result==CFI_ERR_OK)
    puts("LOG: cficInit works");
  else
    puts("LOG: CFI module failed to start");

  puts("PG_GETARG_CSTRING");
  char* pch;
  pch=PG_GETARG_CSTRING(0);
  // int32 edge= PG_GETARG_INT32(0);
  char path[100];
  sprintf(path,"/home/zhibin/models/%s.fbm",pch);
  puts(path);

  
  char *model_name_full;
  int32 n; 
  if( cficFileModelOpen(path)!=CFI_ERR_OK){//

    puts("LOG: open model failed");
  
  }
  else{
    puts("LOG: open model success");
    cficGetModelName(&model_name_full);
    printf("model name is %s\n",model_name_full);
    cficGetModelEntityTotal(CFI_TYPE_LINE,CFI_SUBTYPE_ALL,&n);
    printf("there are %d lines\n", n);
    FILE *fs = fopen("/tmp/edgenumber.csv","a+");
    fprintf(fs,"%d\n",n);
    fclose(fs);
    
  }

  PG_RETURN_INT32(n); 

}

typedef struct node
{
    char edge[20];
    char face[20];
    struct node *next;
} node;

typedef struct node *list;

typedef struct node3
{
    char edge[20];
    char face1[20];
    char face2[20];
    struct node3 *next;
} node3;
typedef struct node3 *list3; // list is pointer of node structure

typedef struct geomnode
{
    char face[20];
    char geom[20];
    struct geomnode *next;
} geomnode;
typedef struct geomnode *geomlist; // list is pointer of node structure


/*************************************************
Function: full_edge
Description: 
Useage:

CREATE FUNCTION full_edge(cstring) RETURNS bool AS 'cad.so' LANGUAGE C;

select full_edge('evaluate');

select full_edge('import');
CREATE unlogged TABLE IF NOT EXISTS full_edge ( EDGE INTEGER, face1 INTEGER, FACE2 INTEGER , DIHEDRAL INTEGER);
COPY full_edge FROM '/tmp/full_edge.csv' with csv;

*************************************************/

PG_FUNCTION_INFO_V1( full_edge );
Datum full_edge(PG_FUNCTION_ARGS);
Datum full_edge(PG_FUNCTION_ARGS)
{ 
  int line_number,line_list_size=500000,line_number_returned,line_number_of_parents;
  int *line_list_of_entity_numbers,*line_signed_list_of_entity_numbers,*line_list_of_entity_types;
  int class;
  int magic, i;

  char* pch;
  pch=PG_GETARG_CSTRING(0);

  if(strcmp(pch,"evaluate")==0){
    struct timespec vartime = timer_start();  // begin a timer called 'vartime'

    FILE *fs = fopen("/tmp/evaluate_full_edge.csv","w");
    cficGetModelEntityTotal(CFI_TYPE_LINE,CFI_SUBTYPE_ALL,&line_number);
    magic=0;
    cficGetModelEntityList(CFI_TYPE_LINE,CFI_SUBTYPE_ALL,line_list_size,&magic,&line_number_returned,&line_list_of_entity_numbers);

    
    cficGetTopoParentTotal (CFI_TYPE_LINE, line_list_of_entity_numbers[0], &line_number_of_parents);
    magic=0;
    cficGetTopoParentList(CFI_TYPE_LINE,line_list_of_entity_numbers[0],line_list_size,&magic,&line_number_returned,&line_list_of_entity_types,&line_signed_list_of_entity_numbers);
    classify(abs(line_list_of_entity_numbers[0]), &class);   
    
    long time_elapsed_nanos = timer_end(vartime);
    printf("Time taken (nanoseconds): %ld\n",time_elapsed_nanos);

    fprintf (fs, "%ld\n", time_elapsed_nanos);
    fclose (fs);

    return true;

  }
  else if(strcmp(pch,"import")==0){
    FILE *fs = fopen("/tmp/full_edge.csv","w");
    cficGetModelEntityTotal(CFI_TYPE_LINE,CFI_SUBTYPE_ALL,&line_number);
    magic=0;
    cficGetModelEntityList(CFI_TYPE_LINE,CFI_SUBTYPE_ALL,line_list_size,&magic,&line_number_returned,&line_list_of_entity_numbers);

    for(i=0 ;i<line_number;i++){
      cficGetTopoParentTotal (CFI_TYPE_LINE, line_list_of_entity_numbers[i], &line_number_of_parents);
      magic=0;
      cficGetTopoParentList(CFI_TYPE_LINE,line_list_of_entity_numbers[i],line_list_size,&magic,&line_number_returned,&line_list_of_entity_types,&line_signed_list_of_entity_numbers);
      classify(abs(line_list_of_entity_numbers[i]), &class);
      fprintf (fs, "%d,%d,%d,%d\n%d,%d,%d,%d\n",
        (line_list_of_entity_numbers[i]),abs(line_signed_list_of_entity_numbers[0]), abs(line_signed_list_of_entity_numbers[1]),class,
        (line_list_of_entity_numbers[i]),abs(line_signed_list_of_entity_numbers[1]), abs(line_signed_list_of_entity_numbers[0]),class);

    }
    fclose (fs);
    return true;

  }

}

PG_FUNCTION_INFO_V1( face_geom );
Datum face_geom(PG_FUNCTION_ARGS);
Datum face_geom(PG_FUNCTION_ARGS)
{ 

  int number_face, magic, i, face_list_size=500000,face_number_returned,face_signed_geom,face_geom_subtype;
  int* face_list_of_entity_numbers;
  FILE *fp ;
  fp = fopen("/tmp/face_geom.csv","w");

  cficGetModelEntityTotal(CFI_TYPE_FACE,CFI_SUBTYPE_ALL,&number_face);
  magic=0;
  cficGetModelEntityList(CFI_TYPE_FACE,CFI_SUBTYPE_ALL,face_list_size,&magic,&face_number_returned,&face_list_of_entity_numbers);

  //printf("in face_geom %d\n", number_face);
  for (i = 0; i < number_face; i++) {
   // storage all the face geom type
    cficGetTopoEmbed (CFI_TYPE_FACE, face_list_of_entity_numbers[i],&face_signed_geom);
    cficGetGeomSubtype (CFI_TYPE_SURFACE,abs(face_signed_geom), &face_geom_subtype);
      
    fprintf(fp, "%d,%d\n",face_list_of_entity_numbers[i],face_geom_subtype);
    //printf("%d %d\n", face_list_of_entity_numbers[i],face_geom_subtype);

  }

  fclose(fp);
  return true;
}

/*************************************************
Function: draw
Description: SQL function, bounds(edge,face), returns true or false
Others: 
*************************************************/
PG_FUNCTION_INFO_V1( draw );
Datum draw(PG_FUNCTION_ARGS);
Datum draw(PG_FUNCTION_ARGS)
{
  FILE *fp, *fs, *ft;
  fp = fopen("/tmp/result.csv","a+");
  fs = fopen("/tmp/plus","w");
  ft = fopen("/tmp/plus_reslut","w");
  //puts("in draw function");

  char line_buffer[500]; 
    char line_number=0;
    char *p[80]; char *buff;
    int i, ivalue;
    char cmd[100];
    char* entity_name;
   // char* edge;

    sprintf(cmd,"SETA BORE");
    cficCommand(cmd);

  while (fgets(line_buffer, sizeof(line_buffer), fp)) { // read a line
        ++line_number;
        //printf("Read result line%4d: %s", line_number, line_buffer);
        line_buffer[strlen(line_buffer)-1]='\0';

        buff = line_buffer;
        i=0;
        while((p[i]=strtok(buff,","))!=NULL) { //break into 3 strings. p[0],p[1],p[2]
            //printf("value %d %s ",i, p[i]);
            ivalue=atoi(p[i]);
            cficGetEntityName(CFI_TYPE_LINE,ivalue,&entity_name);
      sprintf(cmd,"SETA,BORE,%s", entity_name);
      fprintf(fs, cmd);
      fprintf(fs, "\n");
      puts(cmd);
      cficCommand(cmd);

      fprintf(ft, "%s, %s |\n", p[i],cmd);

          i++;
          buff=NULL;
       }
       puts("\n");
       fprintf(ft, "============\n");
  }

     sprintf(cmd,"plus,l,bore\n");
     fprintf(fs, cmd );
       cficCommand(cmd);
       puts(cmd);




    fclose(fs);
  fclose(fp);
  fclose(ft);
  return true;
}


/*************************************************
Function: face_area
Description: 
Useage:

CREATE FUNCTION face_area(cstring) RETURNS bool AS 'cad.so' LANGUAGE C;

select face_area('evaluate');

select face_area('import');
CREATE TABLE IF NOT EXISTS face_area ( face INTEGER, area float);
COPY face_area FROM '/tmp/face_area.csv' with csv;

*************************************************/

PG_FUNCTION_INFO_V1( face_area );
Datum face_area(PG_FUNCTION_ARGS);
Datum face_area(PG_FUNCTION_ARGS)
{ 
  FILE *fp ;
  int number_face, magic, i, face_list_size=500000,face_number_returned;
  int* face_list_of_entity_numbers;
  char* pch;
  double darea,*dcentre_of_mass,*dfirst_moment_about_origin,*dsecond_moment_about_origin,
    *dmoment_of_inertia_about_origin,*dmoment_of_inertia_about_centre_of_mass; 

  pch=PG_GETARG_CSTRING(0);

  if(strcmp(pch,"evaluate")==0){

    struct timespec vartime = timer_start();  

    fp = fopen("/tmp/evaluate_face_area.csv","w");
    cficGetModelEntityTotal(CFI_TYPE_FACE,CFI_SUBTYPE_ALL,&number_face);
    magic=0;
    cficGetModelEntityList(CFI_TYPE_FACE,CFI_SUBTYPE_ALL,face_list_size,&magic,&face_number_returned,&face_list_of_entity_numbers);
    cficCalcFaceMassProperties(face_list_of_entity_numbers[0], -1,-1, &darea, &dcentre_of_mass,&dfirst_moment_about_origin,
      &dsecond_moment_about_origin,&dmoment_of_inertia_about_origin,&dmoment_of_inertia_about_centre_of_mass);
   
    long time_elapsed_nanos = timer_end(vartime);
    printf(" Time taken (nanoseconds): %ld\n",  time_elapsed_nanos);

    fprintf(fp, "%ld\n", time_elapsed_nanos);
    fclose(fp);

    return true;

  }
  else if(strcmp(pch,"import")==0){
   fp = fopen("/tmp/face_area.csv","w");
   cficGetModelEntityTotal(CFI_TYPE_FACE,CFI_SUBTYPE_ALL,&number_face);
   magic=0;
   cficGetModelEntityList(CFI_TYPE_FACE,CFI_SUBTYPE_ALL,face_list_size,&magic,&face_number_returned,&face_list_of_entity_numbers);


   double darea,*dcentre_of_mass,*dfirst_moment_about_origin,*dsecond_moment_about_origin,
   *dmoment_of_inertia_about_origin,*dmoment_of_inertia_about_centre_of_mass; 


   for( i = 0; i < number_face; i++){

     cficCalcFaceMassProperties(face_list_of_entity_numbers[i], -1,-1, &darea, &dcentre_of_mass,&dfirst_moment_about_origin,
      &dsecond_moment_about_origin,&dmoment_of_inertia_about_origin,&dmoment_of_inertia_about_centre_of_mass);

     fprintf(fp, "%d,%f\n",face_list_of_entity_numbers[i],darea);


   }

   fclose(fp);
   return true;
 }

}

typedef struct entity
{
  char name[20];
  char type[20];
  struct entity *next;
} entity;

typedef struct entity *entitylist;

typedef struct snode
{
  char id[20];
  struct node *next;
} snode;

typedef struct snode *slist;

/*************************************************
Function: face_area
Description: 
Useage:

CREATE FUNCTION translate(cstring) RETURNS cstring AS 'cad.so' LANGUAGE C;

select translate('buttress');
select translate('rib');




*************************************************/

PG_FUNCTION_INFO_V1( translate );
Datum translate(PG_FUNCTION_ARGS);
Datum translate(PG_FUNCTION_ARGS)
{
  FILE* fp;
  char* pch;
  char path[100];
  char line_buffer[100]; 
  char line_number=0;
  char *p[100]; char *buff;
  int i, j;

//////////////////////////////////////////////////////////
// 0. extra from table
  int geomtablenumber=0;
  char featurename[100];
//How to extend?
//1. define new predicate flag variable
//2. Define and allocate memeory of char array[] to put the fragment cmd  

  bool equalflage=false;
  bool geomflage=false;
  bool maxarea=false;
  bool parallel=false;  

  char cmd_equal[20000]="";  
  char cmd_geom[20000]="";    
  char cmd_maxarea[20000]="";  
  char cmd_parallel[20000]="";


//bounds variables
  bool boundsflage=false;
  char cmd_bounds[20000]=""; 

  bool dihedralflage=false;
  char cmd_dihedral[20000]="";  

  bool face_shape_flag=false;
  char cmd_shape_face[20000]=""; 

  struct snode* face_shape_head, *face_shape_curr, *face_shape_temp;  //geom
//export variables 
  bool export_flag=false;
  char cmd_export[20000]="CREATE TABLE RESULT AS SELECT distinct\n"; 
  struct snode* export_head, *export_curr, *export_temp;  



  char selectclause[10000]="CREATE TABLE RESULT AS SELECT \n"; //CREATE TABLE RESULT AS 
  char targetlist[10000]=""; // used for group by statement
  char agglist[10000]=""; // used for group by statement
  char fromclause[10000]="\nFROM\n";
  char whereclause[2000000]="\nWHERE\n";
  static char query[2000000]="";

  bool issinglequery=true;
  char selectclause2[10000]="CREATE TABLE RESULT AS SELECT *\n"; //CREATE TABLE RESULT AS 
  char fromclause2[10000]="FROM result";
  char whereclause2[2000000]="\nWHERE\n";
  static char query2[2000000]="";
  
  struct entity* entityhead, *entitycurr, *entitytemp; 
  pch=PG_GETARG_CSTRING(0);

  
  sprintf(path,"/home/zhibin/defs/%s.csv",pch);
  puts(path);
  fp=fopen(path,"a+"); 

  char* shapelist[100]; int si=0, tsi; bool isshape=false;

//3. create a new predicate list 

    //store entity type info
  
  entityhead=(entity*)malloc(sizeof(entity));
  entitycurr=entityhead;


    int full_edge_number=0; // bounds to full_edge
    struct node* head, *curr, *temp; 
    head=(node*)malloc(sizeof(node));
    curr=head;

    struct geomnode* geomhead, *geomcurr, *geomtemp;  //geom
    geomhead=(geomnode*)malloc(sizeof(geomnode));
    geomcurr=geomhead;
//4. 
    
/////////////////start of while
    while (fgets(line_buffer, sizeof(line_buffer), fp)) { // read a line
      ++line_number;
    //  printf("Read line%4d: %s", line_number, line_buffer);
      line_buffer[strlen(line_buffer)-1]='\0';

      buff = line_buffer;
      i=0;
        while((p[i]=strtok(buff," :,()"))!=NULL) { //break into 3 strings. p[0],p[1],p[2]
         i++;
         buff=NULL;
       }

       if(strcmp(p[0],"define")==0&&strcmp(p[2],"as")==0){ 
        strcpy(featurename,p[1]); 
      //  printf("feature name %s\n", featurename);  
        continue;  
      }


      if (strcmp(p[0],"face")==0||strcmp(p[0],"edge")==0||strcmp(p[0],"vertex")==0){
     //   printf("type line, i=%d\n",i);

        while(i>2){
       //   printf("p[%d]=%s\n",i-1,p[i-1]);
          i--;
          entitytemp=(entity*)malloc(sizeof(entity));
          strcpy(entitytemp->name, p[i]);
          strcpy(entitytemp->type,p[1]);
          entitytemp->next=NULL;
          entitycurr->next=entitytemp;
          entitycurr=entitytemp;  
          if(entityhead==NULL)
            entityhead=entitycurr;        
          p[i]='\0';
        }
        continue;

      }

      if(strcmp(p[0],"satisfying")==0){
        continue;
      }

//4. readin predicates, put data into a list 
      if(strcmp(p[0],"bounds")==0){ 
      //  puts("start to read bounds"); 
        boundsflage=true;
        temp=(node*)malloc(sizeof(node)); 
        strcpy(temp->edge, p[1]);
        strcpy(temp->face,p[2]);
        temp->next=NULL;
        curr->next=temp;
        curr=temp;  
        if(head==NULL)
          head=curr;        
      }
///ends bounds check  
      else if(strcmp(p[0],"dihedral")==0){ 
        dihedralflage=true;
        sprintf(cmd_dihedral,"%sfull_edge_%s.dihedral=%s AND\n",cmd_dihedral,p[1],p[2]);
      }
      else if(strcmp(p[0],"<")==0){ 
        equalflage=true;
        sprintf(cmd_equal,"%s full_edge_%s.edge<full_edge_%s.edge AND\n",cmd_equal,p[0],p[2]);
      }
      else if(strcmp(p[0],"area")==0){   //example for direct translate
          sprintf(fromclause,"%sface_area %s,\n", fromclause, p[1]);
          sprintf(whereclause,"%s%s.area>%s AND\n", whereclause,p[1],p[2]); 
          continue;
        }
      else if(strcmp(p[0],"shape_face")==0){   //example for direct translate
          isshape=true;
          shapelist[si]=malloc(sizeof(char)*20);
          strcpy(shapelist[si], p[1]); 
          si++;    
      }
       
       else if(strcmp(p[0],"export")==0){   //example for direct translate   
         puts("in export"); 
         if(export_flag==false){ //generate face shape list         
          export_head=(snode*)malloc(sizeof(snode));
          export_curr=export_head;
          export_flag=true;
        }
        while(i>1){            
         i--;
         export_temp=(snode*)malloc(sizeof(snode)); 
         strcpy(export_temp->id, p[i]);
         export_temp->next=NULL;
         export_curr->next=export_temp;
         export_curr=export_temp;  
         if(export_head==NULL)
          export_head=export_curr;        
        p[i]='\0';
      }
      continue;
    }



  }
//////////////end of reading  


  puts("end of reading"); 

  if(boundsflage){ //start to turn bounds relation into full_edge relation
   // show bounds data read from csv file. 
    // curr=head->next;
    // while(curr){
    //   printf("2:%s %s\n", curr->edge, curr->face);
    //   curr=curr->next;
    // }


//////////////////////////////////////////////////////////////////////////
//create triple relation
    list l=head, templist;
    struct node3* head3, *curr3, *temp3;
    head3=(node3*)malloc(sizeof(node3));
    curr3=head3;

    while(l){
    //    printf("%s %s\n", l->edge, l->face);
      templist=l;
     //   printf("%s %s\n", q->edge, q->face);

      while(templist){
       if(strcmp(templist->edge,l->edge)==0&&strcmp(templist->face,l->face)!=0){

        temp3=(node3*)malloc(sizeof(node3)); 
        strcpy(temp3->edge, templist->edge);
        strcpy(temp3->face1,l->face);
        strcpy(temp3->face2,templist->face);

        temp3->next=NULL;
        curr3->next=temp3;
        curr3=temp3;  
        if(head3->next==NULL)
          head3=curr3;
              full_edge_number++;//this is to confirm how many from tables
            }
         // printf(" q %s %s\n", q->edge, q->face);
            templist=templist->next;
          }

          l=l->next;
        }

// ////////////////////////////////////////////////////////////////////////

    char* facelist[100];
    bool isexist=false; 
    i=0;
     // show triple relations , j always points to tail of the array
        curr3=head3->next;
        while(curr3){
          printf("list:%s %s %s\n", curr3->edge, curr3->face1,curr3->face2);
          sprintf(selectclause,"%sfull_edge_%s as %s,\n",selectclause, curr3->edge, curr3->edge ); 
          sprintf(targetlist,"%sfull_edge_%s,\n",targetlist, curr3->edge);  

          //generating other target list 
          j=i;
          while(facelist[i]){
             i--;            
          }
          i=j;
          while(facelist[i] && strcmp(facelist[i], curr3->face1)!=0){
            i--;            
          }
          if(i==0){
             sprintf(selectclause,"%sfull_edge_%s.face1 as %s,\n",selectclause, curr3->edge, curr3->face1); 
              sprintf(targetlist,"%sfull_edge_%s.face1,\n",targetlist, curr3->edge); 
             facelist[++j]=malloc(sizeof(char)*20);
             strcpy(facelist[j],curr3->face1);   
             
             //if shape agg exists
                if (isshape){
                  tsi=si-1;    
                  while(shapelist[tsi]){
                    if (strcmp(shapelist[tsi],curr3->face1)==0){           
                      sprintf(agglist,"%sface_geometry_is('evaluate', full_edge_%s.face1, 2006) AND\n", agglist,curr3->edge); 
                   }
                    tsi--;}
                 }   
                 //end shape       
          }
          
          i=j;
          while(facelist[i] && strcmp(facelist[i], curr3->face2)!=0){
            i--;
          }
          if(i==0){
            sprintf(selectclause,"%sfull_edge_%s.face2 as %s,\n",selectclause, curr3->edge, curr3->face2); 
             sprintf(targetlist,"%sfull_edge_%s.face2,\n",targetlist, curr3->edge); 
             facelist[++j]=malloc(sizeof(char)*20);
             strcpy(facelist[j],curr3->face2);   
             //shape
             if (isshape){
                tsi=si-1;
                while(shapelist[tsi]){
                    if (strcmp(shapelist[tsi],curr3->face2)==0){           
                        sprintf(agglist,"%sface_geometry_is('evaluate', full_edge_%s.face2, 2006) AND\n", agglist,curr3->edge); 
                    }
                tsi--;}     
              }
              //end shape 
          }
          i=j;       
         // sprintf(targetlist,"%sfull_edge_%s,\n ",selectclause, curr3->edge); 
          curr3=curr3->next;
        }
        // generate shape agg functions
      
////////////////////////////////////////////////////////////////////////
// create joined triple relation. 

        list3 l3=head3, templist3;

        l3=l3->next;
        while(l3){
          templist3=l3;
          while(templist3){
           if(strcmp(l3->face1,templist3->face1)==0&&strcmp(l3->face2,templist3->face2)!=0){
            sprintf(cmd_bounds,"%sfull_edge_%s.face1=full_edge_%s.face1 AND full_edge_%s.edge<>full_edge_%s.edge AND\n",cmd_bounds,l3->edge, templist3->edge,l3->edge, templist3->edge);        
           }
          else if(strcmp(l3->face1,templist3->face2)==0&&strcmp(l3->face2,templist3->face1)!=0){
            sprintf(cmd_bounds,"%sfull_edge_%s.face1=full_edge_%s.face2 AND full_edge_%s.edge<>full_edge_%s.edge AND\n",cmd_bounds,l3->edge,templist3->edge,l3->edge,templist3->edge);
           }
          else if(strcmp(l3->face1,templist3->face2)!=0&&strcmp(l3->face2,templist3->face1)==0){
            sprintf(cmd_bounds,"%sfull_edge_%s.face2=full_edge_%s.face1 AND full_edge_%s.edge<>full_edge_%s.edge AND\n",cmd_bounds,l3->edge,templist3->edge,l3->edge,templist3->edge);
           }
          else if(strcmp(l3->face1,templist3->face1)!=0&&strcmp(l3->face2,templist3->face2)==0){
            sprintf(cmd_bounds,"%sfull_edge_%s.face2=full_edge_%s.face2 AND full_edge_%s.edge<>full_edge_%s.edge AND\n",cmd_bounds,l3->edge,templist3->edge,l3->edge,templist3->edge);
           }
          else if(strcmp(l3->edge,templist3->edge)&&strcmp(l3->face1,templist3->face1)==0&&strcmp(l3->face2,templist3->face2)==0){
            sprintf(cmd_bounds,"%sfull_edge_%s.edge<>full_edge_%s.edge AND\n",cmd_bounds,l3->edge,templist3->edge);
           }
          else if(strcmp(l3->edge,templist3->edge)&&strcmp(l3->face1,templist3->face2)==0&&strcmp(l3->face2,templist3->face1)==0){
           sprintf(cmd_bounds,"%sfull_edge_%s.edge<>full_edge_%s.edge AND\n",cmd_bounds,l3->edge,templist3->edge);
          }
          templist3=templist3->next;
        }

        l3=l3->next;
      }
//  printf("cmd_bounds\n%s\n",cmd_bounds);
// ends bounds rewrite 


}


///////////////////////////////////////////////////////////////////////////////////////////////////////
puts("Concate all query together"); 

int  full_edge_temp=full_edge_number;

selectclause[strlen(selectclause)-2]='\0';




full_edge_temp=full_edge_number;
while(full_edge_temp){
  sprintf(fromclause,"%sfull_edge full_edge_e%d,\n",fromclause,full_edge_temp);
  full_edge_temp--;
}

fromclause[strlen(fromclause)-2]='\0';
//generate where clause

  sprintf(whereclause,"%s%s%s%s%s%s%s%s", whereclause,cmd_bounds,cmd_dihedral ,cmd_equal,cmd_geom
    ,cmd_maxarea ,cmd_parallel, cmd_shape_face); 

whereclause[strlen(whereclause)-4]='\0';

//generate final query
if(isshape){
  targetlist[strlen(targetlist)-2]='\0';
  sprintf(query,"%s%s%s\nGROUP BY\n%s\nHAVING\n%s",selectclause,fromclause,whereclause,targetlist, agglist);
  query[strlen(query)-4]='\0';
  }
else 
  sprintf(query,"%s%s%s;",selectclause,fromclause,whereclause);
puts("QUERY:");
puts(query);


//////////////////////////////////////////////////////////
fclose(fp);
PG_RETURN_CSTRING(query);
  //PG_RETURN_INT32(0); 

}


/*************************************************
Function: calc_area
Description: 
Useage:

CREATE FUNCTION calc_area(int) RETURNS float AS 'cad.so' LANGUAGE C;

select calc_area(1);


*************************************************/

PG_FUNCTION_INFO_V1( calc_area );
Datum calc_area(PG_FUNCTION_ARGS);
Datum calc_area(PG_FUNCTION_ARGS)
{ 
  
  ///tmp/selectivity_%s.csv
  ///tmp/cost_%s.csv
//puts("calc_area");

  int number_face, magic, i, face_list_size=500000,face_number_returned;
  int* face_list_of_entity_numbers;
  int32* pch;
  double darea,*dcentre_of_mass,*dfirst_moment_about_origin,*dsecond_moment_about_origin,
    *dmoment_of_inertia_about_origin,*dmoment_of_inertia_about_centre_of_mass; 

  pch=PG_GETARG_INT32(0);


struct timespec vartime = timer_start();  // begin a timer called 'vartime'
  
   cficCalcFaceMassProperties(pch, -1,-1, &darea, &dcentre_of_mass,&dfirst_moment_about_origin,
   &dsecond_moment_about_origin,&dmoment_of_inertia_about_origin,&dmoment_of_inertia_about_centre_of_mass);

 //  printf("face:%d,area:%f\n",pch,darea); 
     long time_elapsed_nanos = timer_end(vartime);
 // printf("Time taken (nanoseconds): %ld\n",time_elapsed_nanos);

  // printf("FUNCTION calc_area(%d)\n",pch);
   PG_RETURN_FLOAT8(darea);


}

/*************************************************
Function: calc_area_time
Description: 
Useage:

CREATE FUNCTION calc_area_time(int) RETURNS float AS 'cad.so' LANGUAGE C;

select calc_area_time(1);


*************************************************/

PG_FUNCTION_INFO_V1( get_unit );
Datum get_unit(PG_FUNCTION_ARGS);
Datum get_unit(PG_FUNCTION_ARGS)
{ 
  
  ///tmp/selectivity_%s.csv
  ///tmp/cost_%s.csv
// int a=cficGetModelUnits(CFI_TYPE_FACE); 
// printf("cficGetModelUnits=%d\n",a); 

  // PG_RETURN_FLOAT8(darea);


}




/*************************************************
Function: area_in_range
Description: 
Useage:

CREATE FUNCTION area_in_range(cstring, int, cstring, cstring) RETURNS bool AS 'cad.so' LANGUAGE C;

select area_in_range('estimate', 1, '5','10');
select area_in_range('evaluate', 1, '5','10');

*************************************************/

PG_FUNCTION_INFO_V1( area_in_range );
Datum area_in_range(PG_FUNCTION_ARGS);
Datum area_in_range(PG_FUNCTION_ARGS)
{ 
  
  ///tmp/selectivity_%s.csv
  ///tmp/cost_%s.csv
//  puts(" in area_in_range");
  int number_face, magic, i, face_list_size=500000,face_number_returned;
  int* face_list_of_entity_numbers;
  
  double darea,*dcentre_of_mass,*dfirst_moment_about_origin,*dsecond_moment_about_origin,
    *dmoment_of_inertia_about_origin,*dmoment_of_inertia_about_centre_of_mass; 

  char* pch;
  pch=PG_GETARG_CSTRING(0);
  int32 pch1;
  pch1=PG_GETARG_INT32(1);

  char* pch2;
  pch2=PG_GETARG_CSTRING(2);
  char* pch3;
  pch3=PG_GETARG_CSTRING(3);

  float larea=(float)atof(pch2);
  float harea=(float)atof(pch3);

 // printf("larea=%f,harea=%f",larea,harea);

  if(strcmp(pch,"estimate")==0){

   puts("<<1>> evaluate cost"); 
  struct timespec vartime = timer_start();  // begin a timer called 'vartime'
   cficCalcFaceMassProperties(pch1, -1,-1, &darea, &dcentre_of_mass,&dfirst_moment_about_origin,
   &dsecond_moment_about_origin,&dmoment_of_inertia_about_origin,&dmoment_of_inertia_about_centre_of_mass);
 
  long time_elapsed_nanos = timer_end(vartime);
  printf("Time taken (nanoseconds): %ld\n",time_elapsed_nanos);

  FILE *fp ;
  fp = fopen("/tmp/cost_area_in_range.csv","w");
  fprintf(fp, "%ld\n",time_elapsed_nanos);
  fclose(fp);

  puts("<<2>> estimate selectivity"); 

  char buffer[BUFSIZE+1];
  char q[100];
  Engine *ep;
  
   /*
   * Call engOpen with a NULL string. This starts a MATLAB process 
     * on the current host using the command "matlab".
   */
  if (!(ep = engOpen(""))) {
    fprintf(stderr, "\nCan't start MATLAB engine\n");
    return EXIT_FAILURE;
  }

  engOutputBuffer(ep, buffer, BUFSIZE);
  engEvalString(ep, "cd /tmp/distributions");
  engEvalString(ep, "load face_area_dist.mat");
  sprintf(q,"cdf(pd,log10(%s/165.1541))-cdf(pd,log10(%s/165.1541))",pch3,pch2);
  engEvalString(ep, q);  
  
  printf("%s", buffer);
  puts(q);

  fp = fopen("/tmp/selectivity_area_in_range.csv","w");
  fprintf(fp, "%s\n",buffer);
  fclose(fp);  
 
  engClose(ep);
  
  PG_RETURN_BOOL(true);

}
else{

  // cficCalcFaceMassProperties(pch1, -1,-1, &darea, &dcentre_of_mass,&dfirst_moment_about_origin,
  //  &dsecond_moment_about_origin,&dmoment_of_inertia_about_origin,&dmoment_of_inertia_about_centre_of_mass);
  int face_signed_geom,face_geom_subtype;
  cficGetTopoEmbed (CFI_TYPE_FACE, pch1,&face_signed_geom);
  cficGetGeomSubtype (CFI_TYPE_SURFACE,abs(face_signed_geom), &face_geom_subtype);

  struct timespec vartime = timer_start();  // begin a timer called 'vartime'
   cficCalcFaceMassProperties(pch1, -1,-1, &darea, &dcentre_of_mass,&dfirst_moment_about_origin,
   &dsecond_moment_about_origin,&dmoment_of_inertia_about_origin,&dmoment_of_inertia_about_centre_of_mass);  
  //sleep(1);
  cficCalcFaceMassProperties(pch1, -1,-1, &darea, &dcentre_of_mass,&dfirst_moment_about_origin,
   &dsecond_moment_about_origin,&dmoment_of_inertia_about_origin,&dmoment_of_inertia_about_centre_of_mass);  
  long time_elapsed_nanos = timer_end(vartime);

  //printf("area:      %f : geom %d:",darea,face_geom_subtype);
 // printf("area , %f,",darea);
 // printf("Time taken (ns), %ld\n",time_elapsed_nanos);
  

  if(darea>larea && darea<harea)
    PG_RETURN_BOOL(true);
  else
    PG_RETURN_BOOL(false);

}

}


/*************************************************
Function: area_in_range
Description: 
Useage:

CREATE FUNCTION perimeter_in_range(cstring, int, cstring, cstring) RETURNS bool AS 'cad.so' LANGUAGE C;

select perimeter_in_range('estimate', 1, '5','10');
select perimeter_in_range('evaluate', 1, '5','10');

*************************************************/

PG_FUNCTION_INFO_V1( perimeter_in_range );
Datum perimeter_in_range(PG_FUNCTION_ARGS);
Datum perimeter_in_range(PG_FUNCTION_ARGS)
{ 
  
  ///tmp/selectivity_%s.csv
  ///tmp/cost_%s.csv
 // puts(" in perimeter_in_range");
  char* pch;
  pch=PG_GETARG_CSTRING(0);
  int32 pch1;
  pch1=PG_GETARG_INT32(1);

  char* pch2;
  pch2=PG_GETARG_CSTRING(2);
  char* pch3;
  pch3=PG_GETARG_CSTRING(3);

  float lperimeter=(float)atof(pch2);
  float hperimeter=(float)atof(pch3);

 // printf("larea=%f,harea=%f",larea,harea);
  int number_of_loops,number_of_lines, list_size=1000, magic=0,number_returned, i;
  int* signed_list_of_lines; 

  double perimeter=0;
  double dlength=0;

 // puts("strcmp ==0");
  if(strcmp(pch,"estimate")==0){

   puts("<<1>> get cost"); 
  struct timespec vartime = timer_start();  // begin a timer called 'vartime'

  cficGetFaceLoopLineTotal (pch1,1, &number_of_lines);
  cficGetFaceLoopLineList (pch1,1, list_size, &magic,&number_returned, &signed_list_of_lines); 
  //printf("face %d, number_returned=%d\n", pch1,number_returned);
   
  for(i=0;i<number_returned;i++){
     cficCalcLineLength(abs(signed_list_of_lines[i]), &dlength); 
     perimeter=perimeter+dlength;
  }

  long time_elapsed_nanos = timer_end(vartime);
  printf("Time taken (nanoseconds): %ld\n",time_elapsed_nanos);

  printf("face = %d, number_of_lines=%d,dlength=%f, perimeter %f\n", pch1,number_of_lines,dlength, perimeter);

  FILE *fp ;
  fp = fopen("/tmp/cost_perimeter_in_range.csv","w");
  fprintf(fp, "%ld\n",time_elapsed_nanos);
  fclose(fp);

  puts("<<2>> get selectivity"); 

  char buffer[BUFSIZE+1];
  char q[100];
  Engine *ep;
  
   /*
   * Call engOpen with a NULL string. This starts a MATLAB process 
     * on the current host using the command "matlab".
   */
  if (!(ep = engOpen(""))) {
    fprintf(stderr, "\nCan't start MATLAB engine\n");
    return EXIT_FAILURE;
  }

  engOutputBuffer(ep, buffer, BUFSIZE);
  engEvalString(ep, "cd /tmp/distributions");
  engEvalString(ep, "load face_perimeters_dist.mat");
  sprintf(q,"cdf(pd,log10(%s/51.0547))-cdf(pd,log10(%s/51.0547))",pch3,pch2);
  engEvalString(ep, q);  
  
  printf("%s", buffer);
  puts(q);

  fp = fopen("/tmp/selectivity_perimeter_in_range.csv","w");
  fprintf(fp, "%s\n",buffer);
  fclose(fp);  
 
  engClose(ep);
  
  PG_RETURN_BOOL(true);

}
else{
  
  struct timespec vartime = timer_start();  // begin a timer called 'vartime'

  number_of_lines=0;
  ERR(cficGetFaceLoopLineTotal (pch1,1, &number_of_lines));// printf("number of lines %d\n", number_of_lines);
  list_size=1000, magic=0,number_returned=0;

 // printf("face %d,list_size=%d, number_returned=%d\n", pch1,list_size,number_returned);
  ERR(cficGetFaceLoopLineList (pch1,1, list_size, &magic,&number_returned, &signed_list_of_lines)); 
 // printf("face %d,list_size=%d, number_returned=%d\n", pch1,list_size,number_returned);
  dlength=0;
  perimeter=0;
  
  for(i=0;i<number_returned;i++){
     ERR(cficCalcLineLength(abs(signed_list_of_lines[i]), &dlength)); 
     perimeter=perimeter+dlength;
  }

  long time_elapsed_nanos = timer_end(vartime);
 // printf("perimeter, %f, ",perimeter);
 // printf("Time taken (ns), %ld\n",time_elapsed_nanos);

  if(perimeter>lperimeter && perimeter<hperimeter)
     PG_RETURN_BOOL(true);
  else
     PG_RETURN_BOOL(false);

    cleanup:
    puts("clean up");
   // cficExit();

}

}
/*************************************************
Function: area_in_range
Description: 
Useage:

CREATE FUNCTION calc_perimeter(int) RETURNS float AS 'cad.so' LANGUAGE C;

select calc_perimeter('estimate', 1, '5','10');
select calc_perimeter('evaluate', 1, '5','10');

*************************************************/

PG_FUNCTION_INFO_V1( calc_perimeter );
Datum calc_perimeter(PG_FUNCTION_ARGS);
Datum calc_perimeter(PG_FUNCTION_ARGS)
{ 
  
  ///tmp/selectivity_%s.csv
  ///tmp/cost_%s.csv
 // puts(" in perimeter_in_range");

  int32 pch1;
  pch1=PG_GETARG_INT32(0);  

  int number_of_loops,number_of_lines, list_size=1000, magic=0,number_returned, i;
  int* signed_list_of_lines; 

  double perimeter=0;
  double dlength=0;
  
  struct timespec vartime = timer_start();  // begin a timer called 'vartime'

  number_of_lines=0;
  ERR(cficGetFaceLoopLineTotal (pch1,1, &number_of_lines));// printf("number of lines %d\n", number_of_lines);
  list_size=1000, magic=0,number_returned=0;


  ERR(cficGetFaceLoopLineList (pch1,1, list_size, &magic,&number_returned, &signed_list_of_lines)); 
  dlength=0;
  perimeter=0;
  
  for(i=0;i<number_returned;i++){
     ERR(cficCalcLineLength(abs(signed_list_of_lines[i]), &dlength)); 
     perimeter=perimeter+dlength;
  }

  long time_elapsed_nanos = timer_end(vartime);
 // printf("perimeter, %f, ",perimeter);
 // printf("Time taken (ns), %ld\n",time_elapsed_nanos);
//puts("calc_perimeter"); 
  

    cleanup:
  //  puts("clean up");
   // cficExit();
    PG_RETURN_FLOAT8(perimeter);


}

/*************************************************
Function: line_length
Description: SQL function, 
Others: 
Useage:

CREATE FUNCTION calc_line_length(int) RETURNS float AS 'cad.so' LANGUAGE C;
select calc_line_length(1);



*************************************************/

PG_FUNCTION_INFO_V1( calc_line_length );
Datum calc_line_length(PG_FUNCTION_ARGS); 
Datum  //  Datum
calc_line_length (PG_FUNCTION_ARGS)
{ 
  int32* pch;
  pch=PG_GETARG_INT32(0);
  double dlength;
  cficCalcLineLength(pch, &dlength);      
  printf("length=%f\n",dlength);
  PG_RETURN_FLOAT8(dlength);
}


/*************************************************
Function: area_in_range
Description: 
Useage:

CREATE FUNCTION length_in_range(cstring, int, cstring, cstring) RETURNS bool AS 'cad.so' LANGUAGE C;

select length_in_range('estimate', 1, '5','10');
select length_in_range('evaluate', 1, '5','10');

*************************************************/

PG_FUNCTION_INFO_V1( length_in_range );
Datum length_in_range(PG_FUNCTION_ARGS);
Datum length_in_range(PG_FUNCTION_ARGS)
{ 
  
  char* pch;
  pch=PG_GETARG_CSTRING(0);
  int32 pch1;
  pch1=PG_GETARG_INT32(1);

  char* pch2;
  pch2=PG_GETARG_CSTRING(2);
  char* pch3;
  pch3=PG_GETARG_CSTRING(3);

  float llength=(float)atof(pch2);
  float hlength=(float)atof(pch3);

  double dlength;
 // printf("larea=%f,harea=%f",larea,harea);

  if(strcmp(pch,"estimate")==0){

   puts("<<1>> evaluate cost"); 
   struct timespec vartime = timer_start();  // begin a timer called 'vartime'   
   
   cficCalcLineLength(pch1, &dlength);
 
   long time_elapsed_nanos = timer_end(vartime);
   printf("Time taken (nanoseconds): %ld\n",time_elapsed_nanos);

  FILE *fp ;
  fp = fopen("/tmp/cost_length_in_range.csv","w");
  fprintf(fp, "%ld\n",time_elapsed_nanos);
  fclose(fp);

  puts("<<2>> estimate selectivity"); 

  char buffer[BUFSIZE+1];
  char q[100];
  Engine *ep;
  
   /*
   * Call engOpen with a NULL string. This starts a MATLAB process 
     * on the current host using the command "matlab".
   */
  if (!(ep = engOpen(""))) {
    fprintf(stderr, "\nCan't start MATLAB engine\n");
    return EXIT_FAILURE;
  }

  engOutputBuffer(ep, buffer, BUFSIZE);
  engEvalString(ep, "cd /home/zhibin/");
  engEvalString(ep, "load edge_length.mat");
  sprintf(q,"cdf(pd,log10(%s))-cdf(pd,log10(%s))",pch3,pch2);
  engEvalString(ep, q);  
  
  printf("%s", buffer);
  puts(q);

  fp = fopen("/tmp/selectivity_length_in_range.csv","w");
  fprintf(fp, "%s\n",buffer);
  fclose(fp);  
 
  engClose(ep);
  
  PG_RETURN_BOOL(true);

}
else{

  cficCalcLineLength(pch1, &dlength);

 // printf("length=%f\n",dlength);

  if(dlength>llength && dlength<hlength)
    PG_RETURN_BOOL(true);
  else
    PG_RETURN_BOOL(false);

}

}




/*************************************************
Function: face_has_vertex_number
Description: 
Useage:

CREATE FUNCTION face_has_vertex_number(int) RETURNS int AS 'cad.so' LANGUAGE C;

select face_has_vertex_number(1);


*************************************************/

PG_FUNCTION_INFO_V1( face_has_vertex_number );
Datum face_has_vertex_number(PG_FUNCTION_ARGS);
Datum face_has_vertex_number(PG_FUNCTION_ARGS)
{ 
  int number_of_loops, j;
  int32 number_of_vertex=0, sum_vertex=0; 
  int32* pch; 

  pch=PG_GETARG_INT32(0);
  cficGetFaceLoopTotal (pch, &number_of_loops); 

  for(j=0;j<number_of_loops;j++){
      cficGetFaceLoopLineTotal (pch, j+1, &number_of_vertex); 
      sum_vertex=sum_vertex+number_of_vertex; 
    }
  printf("FUNCTION face_has_vertex_number(%d)\n",pch);
  PG_RETURN_INT32(sum_vertex);


}

/*************************************************
Function: face_has_loop_number
Description: 
Useage:

CREATE FUNCTION face_has_loop_number(int) RETURNS int AS 'cad.so' LANGUAGE C;

select face_has_loop_number(1);


*************************************************/

PG_FUNCTION_INFO_V1( face_has_loop_number );
Datum face_has_loop_number(PG_FUNCTION_ARGS);
Datum face_has_loop_number(PG_FUNCTION_ARGS)
{ 
  int number_of_loops, j;
  int32 number_of_vertex=0, sum_vertex=0; 
  int32* pch; 

  pch=PG_GETARG_INT32(0);
  cficGetFaceLoopTotal (pch, &number_of_loops); 

  PG_RETURN_INT32(number_of_loops);


}


/*************************************************
Function: face_geometry
Description: SQL function, face_geometry(face int,facetype int)
Others: 
Useage:

CREATE FUNCTION face_geometry() RETURNS bool AS 'cad.so' LANGUAGE C;
select face_geometry();

CREATE unlogged TABLE IF NOT EXISTS face_geometry ( face INTEGER, facetype integer );
COPY face_geometry FROM '/tmp/face_geometry.csv' with csv;

*************************************************/
PG_FUNCTION_INFO_V1( face_geometry );
Datum face_geometry(PG_FUNCTION_ARGS);
Datum  //  Datum
face_geometry(PG_FUNCTION_ARGS)
{ 

  int number_face, magic, i, face_list_size=500000,face_number_returned,face_signed_geom,face_geom_subtype;
  int* face_list_of_entity_numbers;
  FILE *fp ;
  fp = fopen("/tmp/face_geometry.csv","w");

  cficGetModelEntityTotal(CFI_TYPE_FACE,CFI_SUBTYPE_ALL,&number_face);
  magic=0;
  cficGetModelEntityList(CFI_TYPE_FACE,CFI_SUBTYPE_ALL,face_list_size,&magic,&face_number_returned,&face_list_of_entity_numbers);

  //printf("in face_geom %d\n", number_face);
  for (i = 0; i < number_face; i++) {
   // storage all the face geom type
    cficGetTopoEmbed (CFI_TYPE_FACE, face_list_of_entity_numbers[i],&face_signed_geom);
    cficGetGeomSubtype (CFI_TYPE_SURFACE,abs(face_signed_geom), &face_geom_subtype);

    fprintf(fp, "%d,%d\n",face_list_of_entity_numbers[i],face_geom_subtype);
    //printf("%d %d\n", face_list_of_entity_numbers[i],face_geom_subtype);

  }

  fclose(fp);
  return true;
}


/*************************************************
Function: sphere_parameter
Description: SQL function, sphere_parameter(face int,center point, radius double)
Others: 
Useage:

model: Ball_Bearing.fbm

CREATE FUNCTION calc_sphere_center(int) RETURNS real[] AS 'cad.so' LANGUAGE C;
select calc_sphere_center(1);



*************************************************/

PG_FUNCTION_INFO_V1( calc_sphere_center );
Datum calc_sphere_center(PG_FUNCTION_ARGS);
Datum  //  Datum
calc_sphere_center(PG_FUNCTION_ARGS)
{ 

  int32* pch;
  int p1;  
  ArrayType  *p1_xyz;
   double radius;
  ArrayType  *result;

  pch=PG_GETARG_INT32(0);
  cficGetSphereDefn (pch,&p1,&radius); 
  cficGetPointDefn (p1, &p1_xyz); 

  result = construct_array(p1_xyz, 3, FLOAT8OID, sizeof(float8), FLOAT8PASSBYVAL, 'd');

  PG_RETURN_ARRAYTYPE_P(result);

}

/*************************************************
Function: calc_sphere_radius
Others: 
Useage:

model: Ball_Bearing.fbm

CREATE FUNCTION calc_sphere_radius(int) RETURNS real[] AS 'cad.so' LANGUAGE C;
select calc_sphere_radius(1);



*************************************************/

PG_FUNCTION_INFO_V1( calc_sphere_radius );
Datum calc_sphere_radius(PG_FUNCTION_ARGS);
Datum  //  Datum
calc_sphere_radius(PG_FUNCTION_ARGS)
{ 

  int32* pch;
  int p1;  
  ArrayType  *p1_xyz;
   double radius;
  ArrayType  *result;

  pch=PG_GETARG_INT32(0);
  cficGetSphereDefn (pch,&p1,&radius); 
  cficGetPointDefn (p1, &p1_xyz); 

  result = construct_array(p1_xyz, 3, FLOAT8OID, sizeof(float8), FLOAT8PASSBYVAL, 'd');

  PG_RETURN_FLOAT8(radius);

}





/*************************************************
Function: calc_area
Description: 
Useage:

CREATE FUNCTION calc_face_box(int) RETURNS float[] AS 'cad.so' LANGUAGE C;

select calc_face_box(1);

note:
cfi_get_entity_box (<entity_type, <entity_number,
>X_lower_limit, >X_upper_limit,
>Y_lower_limit, >Y_upper_limit,
>Z_lower_limit, >Z_upper_limit, <>result)

*************************************************/

PG_FUNCTION_INFO_V1( calc_face_box );
Datum calc_face_box(PG_FUNCTION_ARGS);
Datum calc_face_box(PG_FUNCTION_ARGS)
{ 

  int32* pch;  
  float8 box[6];
  ArrayType  *result;

  pch=PG_GETARG_INT32(0);

  cficGetEntityBox (CFI_TYPE_FACE, pch, &box[0],&box[1],&box[2],&box[3],&box[4],&box[5]);
 

  result = construct_array(box, 6, FLOAT8OID, sizeof(float8), FLOAT8PASSBYVAL, 'd');

  printf("FUNCTION calc_face_box(%d) %f,%f,%f,%f,%f,%f\n",pch,box[0],box[1],box[2],box[3],box[4],box[5]);
  

  PG_RETURN_ARRAYTYPE_P(result);


}

/*************************************************
Function: calc_face_loop_total
Description: 
Useage:

CREATE FUNCTION calc_face_loop_total(int) RETURNS int AS 'cad.so' LANGUAGE C;

select calc_face_loop_total(1);

note:
cfi_get_face_loop_total (<face_number, >number_of_loops, <>result)


*************************************************/

PG_FUNCTION_INFO_V1( calc_face_loop_total );
Datum calc_face_loop_total(PG_FUNCTION_ARGS);
Datum calc_face_loop_total(PG_FUNCTION_ARGS)
{ 

  int32* pch;  
  int number_of_loops;
  pch=PG_GETARG_INT32(0);
  cficGetFaceLoopTotal(pch,&number_of_loops);
  PG_RETURN_INT32(number_of_loops);

}


/*************************************************
Function: calc_face_uv_box
Description: 
Useage:

CREATE FUNCTION calc_face_uv_box(int) RETURNS float[] AS 'cad.so' LANGUAGE C;

select calc_face_uv_box(1);

note:
cfi_get_face_uv_box (<face_number,
>U_lower_limit, >U_upper_limit,
>V_lower_limit, >V_upper_limit, <>result)


*************************************************/

PG_FUNCTION_INFO_V1( calc_face_uv_box );
Datum calc_face_uv_box(PG_FUNCTION_ARGS);
Datum calc_face_uv_box(PG_FUNCTION_ARGS)
{ 

  int32* pch;  
  float8 box[4];
  ArrayType  *result;

  pch=PG_GETARG_INT32(0);

  cficGetFaceUVBox(pch, &box[0],&box[1],&box[2],&box[3]);
 

  result = construct_array(box, 4, FLOAT8OID, sizeof(float8), FLOAT8PASSBYVAL, 'd');

  printf("FUNCTION calc_face_box(%d) %f,%f,%f,%f\n",pch,box[0],box[1],box[2],box[3]);
  

  PG_RETURN_ARRAYTYPE_P(result);


}

/*************************************************
Function: calc_area
Description: 
Useage:

CREATE FUNCTION calc_edge_box(int) RETURNS float[] AS 'cad.so' LANGUAGE C;

select calc_edge_box(1);

note:
cfi_get_entity_box (<entity_type, <entity_number,
>X_lower_limit, >X_upper_limit,
>Y_lower_limit, >Y_upper_limit,
>Z_lower_limit, >Z_upper_limit, <>result)

*************************************************/

PG_FUNCTION_INFO_V1( calc_edge_box );
Datum calc_edge_box(PG_FUNCTION_ARGS);
Datum calc_edge_box(PG_FUNCTION_ARGS)
{ 

  int32* pch;  
  float8 box[6];
  ArrayType  *result;

  pch=PG_GETARG_INT32(0);

  cficGetEntityBox (CFI_TYPE_LINE, pch, &box[0],&box[1],&box[2],&box[3],&box[4],&box[5]);
 

  result = construct_array(box, 6, FLOAT8OID, sizeof(float8), FLOAT8PASSBYVAL, 'd');

  printf("FUNCTION calc_face_box(%d) %f,%f,%f,%f,%f,%f\n",pch,box[0],box[1],box[2],box[3],box[4],box[5]);
  

  PG_RETURN_ARRAYTYPE_P(result);


}

/*************************************************
Function: calc_face_uv_box
Description: 
Useage:

CREATE FUNCTION calc_line_t_box(int) RETURNS float[] AS 'cad.so' LANGUAGE C;

select calc_line_t_box(1);

note:
cfi_get_line_t_box (<line_number,
>minimum_t_value, >maximum_t_value, <>result)



*************************************************/

PG_FUNCTION_INFO_V1( calc_line_t_box );
Datum calc_line_t_box(PG_FUNCTION_ARGS);
Datum calc_line_t_box(PG_FUNCTION_ARGS)
{ 

  int32* pch;  
  float8 box[2];
  ArrayType  *result;

  pch=PG_GETARG_INT32(0);

  cficGetLineTBox(pch, &box[0],&box[1]);
 

  result = construct_array(box, 2, FLOAT8OID, sizeof(float8), FLOAT8PASSBYVAL, 'd');

  printf("FUNCTION calc_face_box(%d) %f,%f\n",pch,box[0],box[1]);
  

  PG_RETURN_ARRAYTYPE_P(result);


}

/*************************************************
Function: calc_area
Description: 
Useage:

CREATE FUNCTION calc_vertex_box(int) RETURNS float[] AS 'cad.so' LANGUAGE C;

select calc_vertex_box(1);

note:
cfi_get_entity_box (<entity_type, <entity_number,
>X_lower_limit, >X_upper_limit,
>Y_lower_limit, >Y_upper_limit,
>Z_lower_limit, >Z_upper_limit, <>result)

*************************************************/

PG_FUNCTION_INFO_V1( calc_vertex_box );
Datum calc_vertex_box(PG_FUNCTION_ARGS);
Datum calc_vertex_box(PG_FUNCTION_ARGS)
{ 

  int32* pch;  
  float8 box[6];
  ArrayType  *result;

  pch=PG_GETARG_INT32(0);
  cficGetEntityBox (CFI_TYPE_POINT, pch, &box[0],&box[1],&box[2],&box[3],&box[4],&box[5]);
  result = construct_array(box, 6, FLOAT8OID, sizeof(float8), FLOAT8PASSBYVAL, 'd');
  PG_RETURN_ARRAYTYPE_P(result);
}

/*************************************************
Function: calc_area
Description: 
Useage:

CREATE FUNCTION calc_body_box(int) RETURNS float[] AS 'cad.so' LANGUAGE C;

select calc_body_box(1);

note:
cfi_get_entity_box (<entity_type, <entity_number,
>X_lower_limit, >X_upper_limit,
>Y_lower_limit, >Y_upper_limit,
>Z_lower_limit, >Z_upper_limit, <>result)

*************************************************/

PG_FUNCTION_INFO_V1( calc_body_box );
Datum calc_body_box(PG_FUNCTION_ARGS);
Datum calc_body_box(PG_FUNCTION_ARGS)
{ 

  int32* pch;  
  float8 box[6];
  ArrayType  *result;

  pch=PG_GETARG_INT32(0);
  cficGetEntityBox (CFI_TYPE_BODY, pch, &box[0],&box[1],&box[2],&box[3],&box[4],&box[5]);
  result = construct_array(box, 6, FLOAT8OID, sizeof(float8), FLOAT8PASSBYVAL, 'd');
  PG_RETURN_ARRAYTYPE_P(result);
}


/*************************************************
Function: calc_cylinder_axis
Description: 
Useage:

CREATE FUNCTION calc_cylinder_axis(int) RETURNS float[] AS 'cad.so' LANGUAGE C;

select calc_cylinder_axis(7);

note:
cfi_get_cylinder_defn (<surface_number,
>point_axis_1, >point_axis_2, >radius,
<>result)

*************************************************/

PG_FUNCTION_INFO_V1( calc_cylinder_axis );
Datum calc_cylinder_axis(PG_FUNCTION_ARGS);
Datum calc_cylinder_axis(PG_FUNCTION_ARGS)
{ 

  int32* pch;  
  float8 v[4];
  ArrayType  *result;

  double radius;
  double *p1_xyz,*p2_xyz,*p3_xyz;
  double v_sqr;

  int p1,p2,p3; 

  pch=PG_GETARG_INT32(0);

  cficGetCylinderDefn(pch, &p1,&p2,&radius);
  cficGetPointDefn (p1, &p1_xyz); 
  cficGetPointDefn (p2, &p2_xyz); 

   v[0]=p2_xyz[0]-p1_xyz[0];
   v[1]=p2_xyz[1]-p1_xyz[1];
   v[2]=p2_xyz[2]-p1_xyz[2];

   v_sqr=sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
   v[0]=v[0]/v_sqr;
   v[1]=v[1]/v_sqr;
   v[2]=v[2]/v_sqr;
   v[3]=radius;

  result = construct_array(v, 3, FLOAT8OID, sizeof(float8), FLOAT8PASSBYVAL, 'd');  

  PG_RETURN_ARRAYTYPE_P(result);


}


/*************************************************
Function: calc_cylinder_radius
Description: 
Useage:

CREATE FUNCTION calc_cylinder_radius(int) RETURNS float AS 'cad.so' LANGUAGE C;

select calc_cylinder_radius(7);

note:
cfi_get_cylinder_defn (<surface_number,
>point_axis_1, >point_axis_2, >radius,
<>result)

*************************************************/

PG_FUNCTION_INFO_V1( calc_cylinder_radius );
Datum calc_cylinder_radius(PG_FUNCTION_ARGS);
Datum calc_cylinder_radius(PG_FUNCTION_ARGS)
{ 

  int32* pch;  
  double radius;
  int p1,p2; 

  pch=PG_GETARG_INT32(0);
  cficGetCylinderDefn(pch, &p1,&p2,&radius); 
  PG_RETURN_FLOAT8(radius);

}

/*************************************************
Function: calc_cylinder_radius
Description: 
Useage:

CREATE FUNCTION calc_cylinder_within_direction(int,float[]) RETURNS float AS 'cad.so' LANGUAGE C;

select calc_cylinder_within_direction(8,array[1,2,3]);

note:
cfi_get_cylinder_defn (<surface_number,
>point_axis_1, >point_axis_2, >radius,
<>result)

*************************************************/

PG_FUNCTION_INFO_V1( calc_cylinder_within_direction );
Datum calc_cylinder_within_direction(PG_FUNCTION_ARGS);
Datum calc_cylinder_within_direction(PG_FUNCTION_ARGS)
{ 

  int32* pch;  
  float8 v[3],a[3];
  ArrayType  *result;
 

  double radius;
  double *p1_xyz,*p2_xyz,*p3_xyz;
  double v_sqr;
  int p1,p2,p3; 

  ArrayType* pch_array; 
  Datum    *key_datums;
  bool     *key_nulls;
  int     key_count;
  int16   elmlen;
  bool    elmbyval;
  char    elmalign;
  
  pch=PG_GETARG_INT32(0);
  pch_array=PG_GETARG_ARRAYTYPE_P(1); 


  get_typlenbyvalalign(ARR_ELEMTYPE(pch_array),
             &elmlen, &elmbyval, &elmalign);
    deconstruct_array(pch_array,
              ARR_ELEMTYPE(pch_array),elmlen, elmbyval,elmalign,
              &key_datums, &key_nulls, &key_count);  


  cficGetCylinderDefn(pch, &p1,&p2,&radius);
  cficGetPointDefn (p1, &p1_xyz); 
  cficGetPointDefn (p2, &p2_xyz); 

   v[0]=p2_xyz[0]-p1_xyz[0];
   v[1]=p2_xyz[1]-p1_xyz[1];
   v[2]=p2_xyz[2]-p1_xyz[2];

 //  v_sqr=sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
 //  v[0]=v[0]/v_sqr;
 //  v[1]=v[1]/v_sqr;
 //  v[2]=v[2]/v_sqr;

   a[0]=DatumGetFloat8(key_datums[0]);
   a[1]=DatumGetFloat8(key_datums[1]);
   a[2]=DatumGetFloat8(key_datums[2]);

//a*b=x1x2+y1y2+z1z2

//|a|=√(x1^2+y1^2+z1^2).|b|=√(x2^2+y2^2+z2^2)

//cosθ=a*b/(|a|*|b|)

   double angle=a[0]*v[0]+a[1]*v[0]+a[2]*v[2]; 
   angle=abs(angle)/(sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2])+sqrt(a[0]*a[0]+a[1]*a[1]+a[2]*a[2])); 

   PG_RETURN_FLOAT8(angle);


}





/*************************************************
Function: learn
Description: 
Useage:

CREATE FUNCTION learn(int) RETURNS bool AS 'cad.so' LANGUAGE C;
select learn(1);

note:
learn selectivity from models

*************************************************/

PG_FUNCTION_INFO_V1( learn );
Datum learn(PG_FUNCTION_ARGS);
Datum learn(PG_FUNCTION_ARGS)
{

  DIR * dir;
  struct  dirent * ptr;
 struct stat stStatBuf;
 char pFilePath[100]="/home/zhibin/CADfix/sink_train/";
 chdir(pFilePath);
 dir = opendir(pFilePath);
 int number_of_models=0;
 char modelpath[100];
 double max_area, min_area;


 while ((ptr = readdir(dir)) != NULL)
 {
  if (stat(ptr->d_name, &stStatBuf) == -1)
  {
   printf("Get the stat error on file:%s/n", ptr->d_name);
   continue;
 }
 // if ((stStatBuf.st_mode & S_IFDIR) && strcmp(ptr->d_name, ".") != 0 && strcmp(ptr->d_name, "..") != 0)
 // {
 //   char Path[100];
 //   strcpy(Path, pFilePath);
 //   strncat(Path, "/", 1);
 //   strcat(Path, ptr->d_name);
 //   findAllFile(Path);
 // } 
 if (stStatBuf.st_mode & S_IFREG) 
 {
   printf("  %s %d\n", ptr->d_name,number_of_models);
   number_of_models++; 
   sprintf(modelpath,"%s%s",pFilePath,ptr->d_name);
   puts(modelpath);
   open_model(modelpath);

   learn_area();
   learn_perimeter();

   close_model(); 
 }
  //this must change the directory , for maybe changed in the recured
 chdir(pFilePath);
} 
closedir(dir);
printf("training set has %d models\n",number_of_models); 
printf("max area is %f, min area is %f\n", max_area,min_area );

}

void learn_area(){

   int number_face, magic=0, i, face_list_size=500000,face_number_returned;
   int* face_list_of_entity_numbers;

   double darea,*dcentre_of_mass,*dfirst_moment_about_origin,*dsecond_moment_about_origin,
   *dmoment_of_inertia_about_origin,*dmoment_of_inertia_about_centre_of_mass; 
 

   cficGetModelEntityTotal(CFI_TYPE_FACE,CFI_SUBTYPE_ALL,&number_face);
   cficGetModelEntityList(CFI_TYPE_FACE,CFI_SUBTYPE_ALL,face_list_size,&magic,&face_number_returned,&face_list_of_entity_numbers);


   FILE *fp ;  

   for( i = 0; i < number_face; i++){

     struct timespec vartime ;
     vartime = timer_start(); 

     cficCalcFaceMassProperties(face_list_of_entity_numbers[i], -1,-1, &darea, &dcentre_of_mass,&dfirst_moment_about_origin,
      &dsecond_moment_about_origin,&dmoment_of_inertia_about_origin,&dmoment_of_inertia_about_centre_of_mass);

     long time_elapsed_nanos = timer_end(vartime);
    // printf(" Time taken (nanoseconds): %ld\n",  time_elapsed_nanos);
     int face_signed_geom,face_geom_subtype;
     cficGetTopoEmbed (CFI_TYPE_FACE, face_list_of_entity_numbers[i],&face_signed_geom);
     cficGetGeomSubtype (CFI_TYPE_SURFACE,abs(face_signed_geom), &face_geom_subtype);

     if(darea>0){
     fp = fopen("/tmp/face_areas_distribution.csv","a");  
     fprintf(fp, "%d,%d,%f,%ld\n",face_list_of_entity_numbers[i],face_geom_subtype,darea,abs(time_elapsed_nanos));
     fclose(fp);
     }
     else{
      puts("zero face");
     }
     // fp = fopen("/tmp/face_areas_time.csv","a");  
     // fprintf(fp, "%ld\n",time_elapsed_nanos);
     // fclose(fp);

   }


}

void learn_length(){

   int number_edge, magic=0, i, edge_list_size=500000,edge_number_returned;
   int* edge_list_of_entity_numbers;

   cficGetModelEntityTotal(CFI_TYPE_LINE,CFI_SUBTYPE_ALL,&number_edge);
   cficGetModelEntityList(CFI_TYPE_LINE,CFI_SUBTYPE_ALL,edge_list_size,&magic,&edge_number_returned,&edge_list_of_entity_numbers);
 
   FILE *fp ;
   double dlength;

   for( i = 0; i < number_edge; i++){     
     
     struct timespec vartime = timer_start(); 

     cficCalcLineLength(edge_list_of_entity_numbers[i], &dlength);  

     long time_elapsed_nanos = timer_end(vartime);

     fp = fopen("/tmp/edge_lengths.csv","a");  
     fprintf(fp, "%f\n",dlength);
     fclose(fp);

     fp = fopen("/tmp/edge_lengths_time.csv","a");  
     fprintf(fp, "%ld\n",time_elapsed_nanos);
     fclose(fp);

   }

 
}

bool learn_perimeter(){

//add your code to learn value cost 
  puts("in learn perimeter");
   int number_face, i,j, face_list_size=500000,face_number_returned;
   int* face_list_of_entity_numbers;

  FILE *fp ;
  int number_of_loops,number_of_lines, list_size=100, magic=0,number_returned;
  int * signed_list_of_lines; 

  int face_signed_geom,face_geom_subtype;

  double perimeter=0;
  double dlength;

   cficGetModelEntityTotal(CFI_TYPE_FACE,CFI_SUBTYPE_ALL,&number_face);
   cficGetModelEntityList(CFI_TYPE_FACE,CFI_SUBTYPE_ALL,face_list_size,&magic,&face_number_returned,&face_list_of_entity_numbers);
   
   printf("   face number %d\n", number_face );


   for( i = 0; i < number_face; i++){

    // cficGetFaceLoopTotal (face_list_of_entity_numbers[i], &number_of_loops);
     cficGetFaceLoopLineTotal (face_list_of_entity_numbers[i],1, &number_of_lines);
     cficGetFaceLoopLineList (face_list_of_entity_numbers[i],1, list_size, &magic,&number_returned, &signed_list_of_lines); 
     perimeter=0;

     struct timespec vartime = timer_start(); 
       for(j=0;j<number_returned;j++){
         cficCalcLineLength(abs(signed_list_of_lines[j]), &dlength); 
        perimeter+=dlength;
      }
     long time_elapsed_nanos = timer_end(vartime);
     printf("perimeter is %f\n",perimeter);
   
    
     cficGetTopoEmbed (CFI_TYPE_FACE, face_list_of_entity_numbers[i],&face_signed_geom);
     cficGetGeomSubtype (CFI_TYPE_SURFACE,abs(face_signed_geom), &face_geom_subtype);

     if(perimeter>0){
     fp = fopen("/tmp/face_perimeters_distribution.csv","a");  
     fprintf(fp, "%d,%d,%f,%ld\n",face_list_of_entity_numbers[i],face_geom_subtype,perimeter,abs(time_elapsed_nanos));
     fclose(fp);
      }
      else{
        puts("zero perimeter");
      }
     // fp = fopen("/tmp/face_perimeters.csv","a");  
     // fprintf(fp, "%f,\n",perimeter);
     // fclose(fp);
     

     // fp = fopen("/tmp/face_perimeters_time.csv","a");  
     // fprintf(fp, "%ld\n",time_elapsed_nanos);
     // fclose(fp);

   }

   return true;

  
}

/*************************************************
Function: area_in_range
Description: 
Useage:

CREATE FUNCTION face_geometry_is(cstring, int, int) RETURNS bool AS 'cad.so' LANGUAGE C;

select face_geometry_is('estimate', 1, 2006);
select face_geometry_is('evaluate', 1, 2006);

*************************************************/

PG_FUNCTION_INFO_V1( face_geometry_is );
Datum face_geometry_is(PG_FUNCTION_ARGS);
Datum face_geometry_is(PG_FUNCTION_ARGS)
{ 
  
  ///tmp/selectivity_%s.csv
  ///tmp/cost_%s.csv
//  puts(" in area_in_range");
  int number_face, magic, i, face_list_size=500000,face_number_returned;
  int* face_list_of_entity_numbers;
  
  char* pch;
  pch=PG_GETARG_CSTRING(0);
  int32 pch1;
  pch1=PG_GETARG_INT32(1);

  int32 pch2;
  pch2=PG_GETARG_INT32(2);

  if(strcmp(pch,"estimate")==0){

  //  puts("<<1>> evaluate cost"); 
  // struct timespec vartime = timer_start();  // begin a timer called 'vartime'
  //  cficCalcFaceMassProperties(pch1, -1,-1, &darea, &dcentre_of_mass,&dfirst_moment_about_origin,
  //  &dsecond_moment_about_origin,&dmoment_of_inertia_about_origin,&dmoment_of_inertia_about_centre_of_mass);
 
  // long time_elapsed_nanos = timer_end(vartime);
  // printf("Time taken (nanoseconds): %ld\n",time_elapsed_nanos);

  // FILE *fp ;
  // fp = fopen("/tmp/cost_area_in_range.csv","w");
  // fprintf(fp, "%ld\n",time_elapsed_nanos);
  // fclose(fp);

  // puts("<<2>> estimate selectivity"); 

  // char buffer[BUFSIZE+1];
  // char q[100];
  // Engine *ep;
  
   
  //  * Call engOpen with a NULL string. This starts a MATLAB process 
  //    * on the current host using the command "matlab".
   
  // if (!(ep = engOpen(""))) {
  //   fprintf(stderr, "\nCan't start MATLAB engine\n");
  //   return EXIT_FAILURE;
  // }

  // engOutputBuffer(ep, buffer, BUFSIZE);
  // engEvalString(ep, "cd /tmp/distributions");
  // engEvalString(ep, "load face_area_dist.mat");
  // sprintf(q,"cdf(pd,log10(%s/165.1541))-cdf(pd,log10(%s/165.1541))",pch3,pch2);
  // engEvalString(ep, q);  
  
  // printf("%s", buffer);
  // puts(q);

  // fp = fopen("/tmp/selectivity_area_in_range.csv","w");
  // fprintf(fp, "%s\n",buffer);
  // fclose(fp);  
 
  // engClose(ep);
  
  // PG_RETURN_BOOL(true);

}
else{

  struct timespec vartime = timer_start();  // begin a timer called 'vartime'
  int face_signed_geom,face_geom_subtype;
  cficGetTopoEmbed (CFI_TYPE_FACE, pch1,&face_signed_geom);
  cficGetGeomSubtype (CFI_TYPE_SURFACE,abs(face_signed_geom), &face_geom_subtype);
  long time_elapsed_nanos = timer_end(vartime);

  //printf("area:      %f : geom %d:",darea,face_geom_subtype);
  //printf("face type, %d, %d ,",pch1,face_geom_subtype);
 // printf("Time taken (ns), %ld\n",time_elapsed_nanos);
 // printf(" %ld\n",time_elapsed_nanos);
 // usleep(20000);
  if(face_geom_subtype>pch2 && face_geom_subtype<pch2)
    PG_RETURN_BOOL(false);
  else
    PG_RETURN_BOOL(true);

}

}

/*************************************************
Function: area_in_range
Description: 
Useage:

CREATE FUNCTION edge_dihedral_is(cstring, int, int) RETURNS bool AS 'cad.so' LANGUAGE C;

select edge_dihedral_is('estimate', 1, 1);
select edge_dihedral_is('evaluate', 1, 1);

*************************************************/

PG_FUNCTION_INFO_V1( edge_dihedral_is );
Datum edge_dihedral_is(PG_FUNCTION_ARGS);
Datum edge_dihedral_is(PG_FUNCTION_ARGS)
{ 
  
  ///tmp/selectivity_%s.csv
  ///tmp/cost_%s.csv
//  puts(" in area_in_range");
  int number_face, magic, i, face_list_size=500000,face_number_returned;
  int* face_list_of_entity_numbers;
  
  char* pch;
  pch=PG_GETARG_CSTRING(0);
  int32 pch1;
  pch1=PG_GETARG_INT32(1);

  int32 pch2;
  pch2=PG_GETARG_INT32(2);

   int class;

  if(strcmp(pch,"estimate")==0){

  //  puts("<<1>> evaluate cost"); 
  // struct timespec vartime = timer_start();  // begin a timer called 'vartime'
  //  cficCalcFaceMassProperties(pch1, -1,-1, &darea, &dcentre_of_mass,&dfirst_moment_about_origin,
  //  &dsecond_moment_about_origin,&dmoment_of_inertia_about_origin,&dmoment_of_inertia_about_centre_of_mass);
 
  // long time_elapsed_nanos = timer_end(vartime);
  // printf("Time taken (nanoseconds): %ld\n",time_elapsed_nanos);

  // FILE *fp ;
  // fp = fopen("/tmp/cost_area_in_range.csv","w");
  // fprintf(fp, "%ld\n",time_elapsed_nanos);
  // fclose(fp);

  // puts("<<2>> estimate selectivity"); 

  // char buffer[BUFSIZE+1];
  // char q[100];
  // Engine *ep;
  
   
  //  * Call engOpen with a NULL string. This starts a MATLAB process 
  //    * on the current host using the command "matlab".
   
  // if (!(ep = engOpen(""))) {
  //   fprintf(stderr, "\nCan't start MATLAB engine\n");
  //   return EXIT_FAILURE;
  // }

  // engOutputBuffer(ep, buffer, BUFSIZE);
  // engEvalString(ep, "cd /tmp/distributions");
  // engEvalString(ep, "load face_area_dist.mat");
  // sprintf(q,"cdf(pd,log10(%s/165.1541))-cdf(pd,log10(%s/165.1541))",pch3,pch2);
  // engEvalString(ep, q);  
  
  // printf("%s", buffer);
  // puts(q);

  // fp = fopen("/tmp/selectivity_area_in_range.csv","w");
  // fprintf(fp, "%s\n",buffer);
  // fclose(fp);  
 
  // engClose(ep);
  
  // PG_RETURN_BOOL(true);

}
else{

  struct timespec vartime = timer_start();  // begin a timer called 'vartime'
  
  classify(pch1, &class);

  long time_elapsed_nanos = timer_end(vartime);

  //printf("area:      %f : geom %d:",darea,face_geom_subtype);
  printf("edge type, %d, %d \n,",pch1,class);
  printf("Time taken (ns), %ld\n",time_elapsed_nanos);
  

  if( class == pch2)
    PG_RETURN_BOOL(true);
  else
    PG_RETURN_BOOL(false);

}

}

 
/*************************************************
Function: area_in_range
Description: 
Useage:

CREATE FUNCTION octree_cell_from_point(int, int) RETURNS bool AS 'cad.so' LANGUAGE C;

select edge_dihedral_is('estimate', 1, 1);
select edge_dihedral_is('evaluate', 1, 1);

*************************************************/

PG_FUNCTION_INFO_V1( octree_cell_from_point );
Datum octree_cell_from_point(PG_FUNCTION_ARGS);
Datum octree_cell_from_point(PG_FUNCTION_ARGS)
{ 
  int32 octree_number;
  octree_number=PG_GETARG_INT32(0);

  int32 p;
  p=PG_GETARG_INT32(1);

  double *XYZ_coordinates; 
  cficGetPointDefn (p, &XYZ_coordinates); 

  int whether_found, cell_number, data; 

  cficGetOctCellFromXYZ(octree_number, XYZ_coordinates, &whether_found, &cell_number, &data);

  printf("whether_found=%d, cell_number=%d, data=%d\n",whether_found, cell_number, data);
  return true;
}

 


/*************************************************
Function: face_area
Description: 
Useage:

CREATE FUNCTION translate2(cstring) RETURNS cstring AS 'cad.so' LANGUAGE C;
select translate2('side'); 

select translate2('pocket'); 

select translate2('fin'); 


select translate2('roundslot'); 

select translate2('bhole');


select translate2('rib2');


select translate2('buttress');




*************************************************/

PG_FUNCTION_INFO_V1( translate2 );
Datum translate2(PG_FUNCTION_ARGS);
Datum translate2(PG_FUNCTION_ARGS)
{
  FILE* fp;
  char* pch;
  char path[100];
  char line_buffer[100]; 
  char line_number=0;
  char *p[100]; char *buff;
  int i, j;

//////////////////////////////////////////////////////////
// 0. extra from table
  int geomtablenumber=0;
  char featurename[100];
//How to extend?
//1. define new predicate flag variable
//2. Define and allocate memeory of char array[] to put the fragment cmd  

  bool equalflage=false;
  bool geomflage=false;
  bool maxarea=false;
  bool parallel=false;  

  char cmd_equal[20000]="";  
  char cmd_unequal[20000]=""; 
  char cmd_geom[20000]="";    
  char cmd_maxarea[20000]="";  
  char cmd_parallel[20000]="";


//bounds variables
  bool boundsflage=false;
  char cmd_bounds[20000]=""; 

  bool dihedralflage=false;
  char cmd_dihedral[20000]="";  

  bool face_shape_flag=false;
  char cmd_shape_face[20000]=""; 

  struct snode* face_shape_head, *face_shape_curr, *face_shape_temp;  //geom
//export variables 
  bool export_flag=false;
  char cmd_export[20000]="CREATE TABLE RESULT AS SELECT distinct\n"; 
  struct snode* export_head, *export_curr, *export_temp;  



  char selectclause[10000]="CREATE TABLE RESULT AS SELECT \n"; //CREATE TABLE RESULT AS 
  char targetlist[10000]=""; // used for group by statement
  char agglist[10000]=""; // used for group by statement
  char fromclause[10000]="\nFROM\n";
  char whereclause[2000000]="\nWHERE\n";
  static char query[2000000]="";

  bool issinglequery=true;
  char selectclause2[10000]="CREATE TABLE RESULT AS SELECT *\n"; //CREATE TABLE RESULT AS 
  char fromclause2[10000]="FROM result";
  char whereclause2[2000000]="\nWHERE\n";
  static char query2[2000000]="";
  
  struct entity* entityhead, *entitycurr, *entitytemp; 
  pch=PG_GETARG_CSTRING(0);

  
  sprintf(path,"/tmp/defs/%s.csv",pch);
  puts(path);
  fp=fopen(path,"a+"); 

  char* shapelist[100]; int si=0, tsi; bool isshape=false;

//3. create a new predicate list 

    //store entity type info
  
  entityhead=(entity*)malloc(sizeof(entity));
  entitycurr=entityhead;


    int full_edge_number=0; // bounds to full_edge
    struct node* head, *curr, *temp; 
    head=(node*)malloc(sizeof(node));
    curr=head;

    struct geomnode* geomhead, *geomcurr, *geomtemp;  //geom
    geomhead=(geomnode*)malloc(sizeof(geomnode));
    geomcurr=geomhead;
//4. 
    
/////////////////start of while
    while (fgets(line_buffer, sizeof(line_buffer), fp)) { // read a line
      ++line_number;
    //  printf("Read line%4d: %s", line_number, line_buffer);
      line_buffer[strlen(line_buffer)-1]='\0';

      buff = line_buffer;
      i=0;
        while((p[i]=strtok(buff," :,()"))!=NULL) { //break into 3 strings. p[0],p[1],p[2]
         i++;
         buff=NULL;
       }

       if(strcmp(p[0],"define")==0&&strcmp(p[2],"as")==0){ 
        strcpy(featurename,p[1]); 
      //  printf("feature name %s\n", featurename);  
        continue;  
      }


      if (strcmp(p[0],"face")==0||strcmp(p[0],"edge")==0||strcmp(p[0],"vertex")==0){
     //   printf("type line, i=%d\n",i);

        while(i>2){
       //   printf("p[%d]=%s\n",i-1,p[i-1]);
          i--;
          entitytemp=(entity*)malloc(sizeof(entity));
          strcpy(entitytemp->name, p[i]);
          strcpy(entitytemp->type,p[1]);
          entitytemp->next=NULL;
          entitycurr->next=entitytemp;
          entitycurr=entitytemp;  
          if(entityhead==NULL)
            entityhead=entitycurr;        
          p[i]='\0';
        }
        continue;

      }

      if(strcmp(p[0],"satisfying")==0){
        continue;
      }

//4. readin predicates, put data into a list 
      if(strcmp(p[0],"bounds")==0){ 
      //  puts("start to read bounds"); 
        boundsflage=true;
        temp=(node*)malloc(sizeof(node)); 
        strcpy(temp->edge, p[1]);
        strcpy(temp->face,p[2]);
        temp->next=NULL;
        curr->next=temp;
        curr=temp;  
        if(head==NULL)
          head=curr;        
      }
///ends bounds check  
      else if(strcmp(p[0],"dihedral")==0){ 
        dihedralflage=true;
        sprintf(cmd_dihedral,"%sfull_edge_%s.dihedral=%s AND\n",cmd_dihedral,p[1],p[2]);
      }
      else if(strcmp(p[0],"<")==0){ 
        equalflage=true;
        sprintf(cmd_equal,"%s full_edge_%s.edge<full_edge_%s.edge AND\n",cmd_equal,p[0],p[2]);
      }
      else if(strcmp(p[0],"area")==0){   //example for direct translate
          sprintf(fromclause,"%sface_area %s,\n", fromclause, p[1]);
          sprintf(whereclause,"%s%s.area>%s AND\n", whereclause,p[1],p[2]); 
          continue;
        }
      else if(strcmp(p[0],"shape_face")==0){   //example for direct translate
          isshape=true;
          shapelist[si]=malloc(sizeof(char)*20);
          strcpy(shapelist[si], p[1]); 
          si++;    
      }
       
       else if(strcmp(p[0],"export")==0){   //example for direct translate   
         puts("in export"); 
         if(export_flag==false){ //generate face shape list         
          export_head=(snode*)malloc(sizeof(snode));
          export_curr=export_head;
          export_flag=true;
        }
        while(i>1){            
         i--;
         export_temp=(snode*)malloc(sizeof(snode)); 
         strcpy(export_temp->id, p[i]);
         export_temp->next=NULL;
         export_curr->next=export_temp;
         export_curr=export_temp;  
         if(export_head==NULL)
          export_head=export_curr;        
        p[i]='\0';
      }
      continue;
    }



  }
//////////////end of reading  


  puts("end of reading"); 

  if(boundsflage){ //start to turn bounds relation into full_edge relation
   // show bounds data read from csv file. 
    // curr=head->next;
    // while(curr){
    //   printf("2:%s %s\n", curr->edge, curr->face);
    //   curr=curr->next;
    // }


//////////////////////////////////////////////////////////////////////////
//create triple relation
    list l=head, templist;
    struct node3* head3, *curr3, *temp3;
    head3=(node3*)malloc(sizeof(node3));
    curr3=head3;

    while(l){
    //    printf("%s %s\n", l->edge, l->face);
      templist=l;
     //   printf("%s %s\n", q->edge, q->face);

      while(templist){
       if(strcmp(templist->edge,l->edge)==0&&strcmp(templist->face,l->face)!=0){

        temp3=(node3*)malloc(sizeof(node3)); 
        strcpy(temp3->edge, templist->edge);
        strcpy(temp3->face1,l->face);
        strcpy(temp3->face2,templist->face);

        temp3->next=NULL;
        curr3->next=temp3;
        curr3=temp3;  
        if(head3->next==NULL)
          head3=curr3;
              full_edge_number++;//this is to confirm how many from tables
            }
         // printf(" q %s %s\n", q->edge, q->face);
            templist=templist->next;
          }

          l=l->next;
        }

// ////////////////////////////////////////////////////////////////////////

    char* facelist[100];
    bool isexist=false; 
    i=0;
     // show triple relations , j always points to tail of the array
        curr3=head3->next;
        while(curr3){
          printf("list:%s %s %s\n", curr3->edge, curr3->face1,curr3->face2);
          sprintf(selectclause,"%sfull_edge_%s.edge as %s,\n",selectclause, curr3->edge, curr3->edge ); 
          sprintf(targetlist,"%sfull_edge_%s.edge,\n",targetlist, curr3->edge);  

          //generating other target list 
          j=i;
          while(facelist[i]){
             i--;            
          }
          i=j;
          while(facelist[i] && strcmp(facelist[i], curr3->face1)!=0){
            i--;            
          }
          if(i==0){
             sprintf(selectclause,"%sfull_edge_%s.face1 as %s,\n",selectclause, curr3->edge, curr3->face1); 
              sprintf(targetlist,"%sfull_edge_%s.face1,\n",targetlist, curr3->edge); 
             facelist[++j]=malloc(sizeof(char)*20);
             strcpy(facelist[j],curr3->face1);   
             
             //if shape agg exists
                if (isshape){
                  tsi=si-1;    
                  while(shapelist[tsi]){
                    if (strcmp(shapelist[tsi],curr3->face1)==0){           
                      sprintf(agglist,"%sface_geometry_is('evaluate', full_edge_%s.face1, 2006) AND\n", agglist,curr3->edge); 
                   }
                    tsi--;}
                 }   
                 //end shape       
          }
          
          i=j;
          while(facelist[i] && strcmp(facelist[i], curr3->face2)!=0){
            i--;
          }
          if(i==0){
            sprintf(selectclause,"%sfull_edge_%s.face2 as %s,\n",selectclause, curr3->edge, curr3->face2); 
             sprintf(targetlist,"%sfull_edge_%s.face2,\n",targetlist, curr3->edge); 
             facelist[++j]=malloc(sizeof(char)*20);
             strcpy(facelist[j],curr3->face2);   
             //shape
             if (isshape){
                tsi=si-1;
                while(shapelist[tsi]){
                    if (strcmp(shapelist[tsi],curr3->face2)==0){           
                        sprintf(agglist,"%sface_geometry_is('evaluate', full_edge_%s.face2, 2006) AND\n", agglist,curr3->edge); 
                    }
                tsi--;}     
              }
              //end shape 
          }
          i=j;       
         // sprintf(targetlist,"%sfull_edge_%s,\n ",selectclause, curr3->edge); 
          curr3=curr3->next;
        }
        // generate shape agg functions
////////////////////////////////////////////////////////////////////////
// predicates to keep all enities are distinct to each other 25/feb/2015
    char* face_internal[100]; // face entities internal representation, elg.., full_edge_e1.face1 = face1
    char* edge_internal[100]; 
    i=0;
    int k=0; 
     // show triple relations , j always points to tail of the array
        curr3=head3->next;        

        while(curr3){
          printf("list:%s %s %s\n", curr3->edge, curr3->face1,curr3->face2);
          // sprintf(selectclause,"%sfull_edge_%s.edge as %s,\n",selectclause, curr3->edge, curr3->edge ); 
          // sprintf(targetlist,"%sfull_edge_%s,\n",targetlist, curr3->edge);  

          edge_internal[++k]=malloc(sizeof(char)*20);
          sprintf(edge_internal[k],"full_edge_%s.edge",curr3->edge);           
         

          //generating other target list 
          j=i;
          while(facelist[i]){
             i--;            
          }
          i=j;
          while(facelist[i] && strcmp(facelist[i], curr3->face1)!=0){
            i--;            
          }
          if(i==0){
             // sprintf(selectclause,"%sfull_edge_%s.face1 as %s,\n",selectclause, curr3->edge, curr3->face1); 
             // sprintf(targetlist,"%sfull_edge_%s.face1,\n",targetlist, curr3->edge); 
             ++j; 
             facelist[j]=malloc(sizeof(char)*20);
             face_internal[j]=malloc(sizeof(char)*20);
             strcpy(facelist[j],curr3->face1); 
             sprintf(face_internal[j],"full_edge_%s.face1",curr3->edge);       
          }
          
          i=j;
          while(facelist[i] && strcmp(facelist[i], curr3->face2)!=0){
            i--;
          }
          if(i==0){
             ++j;
             facelist[j]=malloc(sizeof(char)*20);
             face_internal[j]=malloc(sizeof(char)*20);
             strcpy(facelist[j],curr3->face2); 
             sprintf(face_internal[j],"full_edge_%s.face2",curr3->edge);    
          }
          i=j;       
         // sprintf(targetlist,"%sfull_edge_%s,\n ",selectclause, curr3->edge); 
          curr3=curr3->next;
        //  printf("i=%d\n",i);
        }
        // generate shape agg functions
  while(face_internal[i]){
    printf("all face internal %s i=%d \n", face_internal[i] , i);
    i--;
  }
   i=9;
  while(face_internal[i]){
    printf("face internal %s\n", face_internal[i] );
    j=i-1;
    while(face_internal[j]){
      sprintf(cmd_unequal,"%s%s<>%s AND\n",cmd_unequal,face_internal[i], face_internal[j] );
     // sprintf(cmd_unequal,"%s%s<>%s AND\n",cmd_unequal,face_internal[j], face_internal[i] );
      printf("%s<>%s AND\n",face_internal[i], face_internal[j]);
      j--;
    }
    i--;
  }
 while(edge_internal[k]){
  //  printf("%s\n",edge_internal[k]);
     j=k-1;
    while(edge_internal[j]){
      sprintf(cmd_unequal,"%s%s<>%s AND\n",cmd_unequal,edge_internal[k], edge_internal[j] );
      j--;
    } 
    k--; 
  }

//puts(cmd_unequal);

///                                                            
////////////////////////////////////////////////////////////////////////
// create joined triple relation. 

        list3 l3=head3, templist3;

        l3=l3->next;
        while(l3){
          templist3=l3;
          while(templist3){
           if(strcmp(l3->face1,templist3->face1)==0&&strcmp(l3->face2,templist3->face2)!=0){
            sprintf(cmd_bounds,"%sfull_edge_%s.face1=full_edge_%s.face1 AND \n",cmd_bounds,l3->edge, templist3->edge);        
           }
          else if(strcmp(l3->face1,templist3->face2)==0&&strcmp(l3->face2,templist3->face1)!=0){
            sprintf(cmd_bounds,"%sfull_edge_%s.face1=full_edge_%s.face2 AND \n",cmd_bounds,l3->edge,templist3->edge);
           }
          else if(strcmp(l3->face1,templist3->face2)!=0&&strcmp(l3->face2,templist3->face1)==0){
            sprintf(cmd_bounds,"%sfull_edge_%s.face2=full_edge_%s.face1 AND \n",cmd_bounds,l3->edge,templist3->edge);
           }
          else if(strcmp(l3->face1,templist3->face1)!=0&&strcmp(l3->face2,templist3->face2)==0){
            sprintf(cmd_bounds,"%sfull_edge_%s.face2=full_edge_%s.face2 AND \n",cmd_bounds,l3->edge,templist3->edge);
           }
          else if(strcmp(l3->edge,templist3->edge)&&strcmp(l3->face1,templist3->face1)==0&&strcmp(l3->face2,templist3->face2)==0){
            sprintf(cmd_bounds,"%sfull_edge_%s.edge<>full_edge_%s.edge AND\n",cmd_bounds,l3->edge,templist3->edge);
           }
          else if(strcmp(l3->edge,templist3->edge)&&strcmp(l3->face1,templist3->face2)==0&&strcmp(l3->face2,templist3->face1)==0){
           sprintf(cmd_bounds,"%sfull_edge_%s.edge<>full_edge_%s.edge AND\n",cmd_bounds,l3->edge,templist3->edge);
          }
          templist3=templist3->next;
        }

        l3=l3->next;
      }
//  printf("cmd_bounds\n%s\n",cmd_bounds);
// ends bounds rewrite 


}


///////////////////////////////////////////////////////////////////////////////////////////////////////
puts("Concate all query together"); 

int  full_edge_temp=full_edge_number;

selectclause[strlen(selectclause)-2]='\0';




full_edge_temp=full_edge_number;
while(full_edge_temp){
  sprintf(fromclause,"%sfull_edge full_edge_e%d,\n",fromclause,full_edge_temp);
  full_edge_temp--;
}

fromclause[strlen(fromclause)-2]='\0';
//generate where clause

  sprintf(whereclause,"%s%s%s%s%s%s%s%s%s", whereclause,cmd_bounds,cmd_dihedral ,cmd_equal,cmd_unequal,cmd_geom
    ,cmd_maxarea ,cmd_parallel, cmd_shape_face); 

whereclause[strlen(whereclause)-4]='\0';

//generate final query
if(isshape){
  targetlist[strlen(targetlist)-2]='\0';
  sprintf(query,"%s%s%s\nGROUP BY\n%s\nHAVING\n%s",selectclause,fromclause,whereclause,targetlist, agglist);
  query[strlen(query)-4]='\0';
  }
else 
  sprintf(query,"%s%s%s;",selectclause,fromclause,whereclause);
puts("QUERY:");
puts(query);


//////////////////////////////////////////////////////////
fclose(fp);
PG_RETURN_CSTRING(query);
  //PG_RETURN_INT32(0); 

}

