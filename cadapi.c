#include "cfiStandardFun.h"
//#include "timers.h"


bool open_model(char* path){

  int mode;
  char *version;
  char *fixno;
  int result = cficInit(&mode, &version, &fixno);
  cficFree(version);
  cficFree(fixno);

  if(result==CFI_ERR_OK)
    puts("LOG: cficInit works");
  else
    puts("LOG: CFI module failed to start");


  char *model_name_full;
  int32 n; 
  if( cficFileModelOpen(path)!=CFI_ERR_OK){//

    puts("LOG: open model failed");

  }
  else{
    puts("LOG: open model success");
    cficGetModelName(&model_name_full);
    printf("model name is %s\n",model_name_full);
    cficGetModelEntityTotal(CFI_TYPE_LINE,CFI_SUBTYPE_ALL,&n);
    printf("there are %d lines\n", n);   
    
  }

  if(n==0)
    return false;

  return true; 

}

bool close_model(){
  cficExit();
  return true;
}



double calc_max_areas(){

   int number_face, magic=0, i, face_list_size=500000,face_number_returned;
   int* face_list_of_entity_numbers;

   double darea,*dcentre_of_mass,*dfirst_moment_about_origin,*dsecond_moment_about_origin,
   *dmoment_of_inertia_about_origin,*dmoment_of_inertia_about_centre_of_mass; 

   double max_area;

   cficGetModelEntityTotal(CFI_TYPE_FACE,CFI_SUBTYPE_ALL,&number_face);
   cficGetModelEntityList(CFI_TYPE_FACE,CFI_SUBTYPE_ALL,face_list_size,&magic,&face_number_returned,&face_list_of_entity_numbers);
   


   for( i = 0; i < number_face; i++){

     cficCalcFaceMassProperties(face_list_of_entity_numbers[i], -1,-1, &darea, &dcentre_of_mass,&dfirst_moment_about_origin,
      &dsecond_moment_about_origin,&dmoment_of_inertia_about_origin,&dmoment_of_inertia_about_centre_of_mass);
     if(darea>max_area)
      max_area=darea;    

   }
   printf("max area %f\n",max_area );
   return max_area;

}

double calc_min_areas(){

   int number_face, magic=0, i, face_list_size=500000,face_number_returned;
   int* face_list_of_entity_numbers;

   double darea,*dcentre_of_mass,*dfirst_moment_about_origin,*dsecond_moment_about_origin,
   *dmoment_of_inertia_about_origin,*dmoment_of_inertia_about_centre_of_mass; 

   double min_area;

   cficGetModelEntityTotal(CFI_TYPE_FACE,CFI_SUBTYPE_ALL,&number_face);
   cficGetModelEntityList(CFI_TYPE_FACE,CFI_SUBTYPE_ALL,face_list_size,&magic,&face_number_returned,&face_list_of_entity_numbers);  


   for( i = 0; i < number_face; i++){

     cficCalcFaceMassProperties(face_list_of_entity_numbers[i], -1,-1, &darea, &dcentre_of_mass,&dfirst_moment_about_origin,
      &dsecond_moment_about_origin,&dmoment_of_inertia_about_origin,&dmoment_of_inertia_about_centre_of_mass);
     if(darea<min_area)
      min_area=darea;    

   }

   printf("min area %f\n", min_area );
   return min_area;

}

bool hist(double *arr_area,int number_area, double *p_area,double max_area,double min_area,double bin_width){

 // puts("hist start"); 
  int bin_size=floor((max_area-min_area)/bin_width);
  int i;
 
  for (i=0; i<bin_size; i++) { 
    p_area[i] = 0; 
  }

 // puts("initialization"); 
  for (i = 0; i < number_area;i++) {
   int index = abs((arr_area[i] - min_area)/bin_width); 
   p_area[index]=p_area[index]+1 ;
 //  printf("p_area[%d]=%f, bin_size=%d, number_area=%d\n",index,p_area[index], bin_size,number_area );
 }

// puts("fill"); 

 for (i = 0; i < bin_size; i++) {    
   p_area[i]=p_area[i]/number_area;
 //  printf("possiblity is %f\n",p_area[i]); 

   char path[100];
   sprintf(path, "/tmp/p_area_%d.csv", i);
  // puts(path);

  if(isnan(p_area[i])||isinf(p_area[i]))
    continue;
  else
  {
     FILE *fs = fopen(path,"a+");
     fprintf(fs,"%f\n",p_area[i]);
     fclose(fs);
  } 

  }

 return true;
}




// bool calc_angle_between_edges(){

//     int number_vertex, magic=0, i, vertex_list_size=500000,vertex_number_returned;
//    int* vertex_list_of_entity_numbers;

//    cficGetModelEntityTotal(CFI_TYPE_POINT,CFI_SUBTYPE_ALL,&number_vertex);
//    cficGetModelEntityList(CFI_TYPE_POINT,CFI_SUBTYPE_ALL,vertex_list_size,&magic,&vertex_number_returned,&vertex_list_of_entity_numbers);
 
  
//    double dlength;
//    int total_relatives;
//    int line_list_size=10;
//    int line_number_returned;
//     int* signed_line_list_of_entity_numbers;
//     int j,k;

//    for( i = 0; i < number_vertex; i++){     
     
//      cficGetTopoRelatedTotal (CFI_TYPE_POINT, vertex_list_of_entity_numbers[i],CFI_TYPE_LINE, CFI_SUBTYPE_ALL,&total_relatives);
//      cficGetTopoRelatedList (CFI_TYPE_POINT,vertex_list_of_entity_numbers[i],CFI_TYPE_LINE, CFI_SUBTYPE_ALL,line_list_size,&magic,
//       &number_returned,&signed_line_list_of_entity_numbers);

//      for(j=0;j<number_returned;j++){
//       for(k=j+1;k<number_returned;k++)
//         calc_angle(signed_line_list_of_entity_numbers[j],signed_line_list_of_entity_numbers[k]);
//      }

//    }

//    return true;

// }
     
// bool calc_angle(int e1, int e2, int p){
//      double dxt1, dyt1, dzt1, dxtt1, dytt1, dztt1;
//      cficCalcLineDerivAtT(e1, 0, dxt1, dyt1, dzt1, dxtt1, dytt1, dztt1); 

//      double dxt2, dyt2, dzt2, dxtt2, dytt2, dztt2;
//      cficCalcLineDerivAtT(e2, 0, dxt2, dyt2, dzt2, dxtt2, dytt2, dztt2);

//      //a*b=x1x2+y1y2+z1z2
//     //|a|=√(x1^2+y1^2+z1^2).|b|=√(x2^2+y2^2+z2^2)
//     //cosθ=a*b/(|a|*|b|)
    
//     double angle=dxt1*dxt2+dyt1*dyt2+dzt1*dzt2;
//     double abs_a=sqrt(dxt1*dxt1+dyt1*dyt1+dzt1*dtz1);
//     double abs_b=sqrt(dxt2*dxt2+dyt2*dyt2+dzt2*dzt2);

//     angle=angle/(abs_a*abs_b);


//      FILE *fp ;
//      fp = fopen("/home/zhibin/logs/edge_angles.csv","a");  
//      fprintf(fp, "%f\n",angle);
//      fclose(fp);

// }



     